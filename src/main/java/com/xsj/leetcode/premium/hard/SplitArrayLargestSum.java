package com.xsj.leetcode.premium.hard;

/**
 * <a href="https://leetcode.com/problems/split-array-largest-sum">410. Split Array Largest Sum</a>.
 * <hr />
 * <p>Given an array which consists of non-negative integers and an integer m, you can split the array into m non-empty
 * continuous subarrays. Write an algorithm to minimize the largest sum among these m subarrays.</p>
 * <p>Note: Given m satisfies the following constraint: 1 ≤ m ≤ length(nums) ≤ 14,000.</p>
 * <pre>
 * Example:
 * Input:
 * nums = [7,2,5,10,8]
 * m = 2
 * Output:
 * 18
 * </pre>
 */
public class SplitArrayLargestSum {


    public int splitArray(int[] nums, int m) {
        int max = 0;
        long sum = 0;
        for (int num : nums) {
            max = Math.max(num, max);
            sum += num;
        }
        if (m == 1) {
            return (int) sum;
        }
        // 最小取值 max
        long l = max;
        // 最大值 sum
        long r = sum;
        while (l <= r) {
            long mid = (l + r) / 2;
            if (valid(mid, nums, m)) {
                r = mid - 1;
            } else  {
                l = mid + 1;
            }
        }
        return (int) l;
    }

    private boolean valid(long target, int[] nums, int m) {
        int count = 1;
        long total = 0;
        for (int num : nums) {
            if (num > target) {
               return false;
            }
            if (total + num > target) {
                ++count;
                total = num;
            } else {
                total += num;
            }
        }
        return count <= m;
    }

}
