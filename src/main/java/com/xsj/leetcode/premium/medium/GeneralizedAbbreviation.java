package com.xsj.leetcode.premium.medium;

import java.util.ArrayList;
import java.util.List;

/**
 * <a href="https://leetcode.com/problems/generalized-abbreviation">320. Generalized Abbreviation</a>.
 * <hr />
 * <p>Write a function to generate the generalized abbreviations of a word.</p>
 * <pre>
 * Example:
 * Given word = "word", return the following list (order does not matter):
 * ["word", "1ord", "w1rd", "wo1d", "wor1", "2rd", "w2d", "wo2", "1o1d", "1or1", "w1r1", "1o2", "2r1", "3d", "w3", "4"]
 * </pre>
 * ref: https://discuss.leetcode.com/category/400/generalized-abbreviation
 *     http://www.cnblogs.com/grandyang/p/5261569.html
 *
 */
public class GeneralizedAbbreviation {

    private GeneralizedAbbreviation() {}

    public static List<String> generateAbbreviations(String word) {
        List<String> res = new ArrayList<>();
        DFS(res, new StringBuilder(), word.toCharArray(), 0, 0);
        return res;
    }

    private static void DFS(List<String> res, StringBuilder builder, char[] c, int i, int num) {
        int len = builder.length();
        if (i == c.length) {
            if(num != 0) builder.append(num);
            res.add(builder.toString());
        } else {
            DFS(res, builder, c, i + 1, num + 1);               // abbr c[i]

            if(num != 0) builder.append(num);                   // not abbr c[i]
            DFS(res, builder.append(c[i]), c, i + 1, 0);
        }
        builder.setLength(len);
    }

}
