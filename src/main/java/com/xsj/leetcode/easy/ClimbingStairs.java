package com.xsj.leetcode.easy;

/**
 * <a href="https://leetcode.com/problems/climbing-stairs/" >70. Climbing Stairs</a>
 */
public class ClimbingStairs {

    /**
     * 1 阶  -> 1
     * 2 阶  -> 2
     * 3 阶  -> (到达 1 阶，直接前进两步 ) + (到达 2 阶，前进一步)
     * 4 阶  -> (到达 3 阶，前进一步) + (到达 2 阶，直接前进两步)
     */
    public static int climbStairs(int n) {
        int pre = 1;
        int cur = 1;
        for (int i = 2; i <= n; i++) {
            int tmp = pre + cur;
            pre = cur;
            cur = tmp;
        }
        return  cur;
    }


}
