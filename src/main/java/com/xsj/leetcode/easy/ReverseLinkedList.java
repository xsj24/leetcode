package com.xsj.leetcode.easy;

/**
 * <a href="https://leetcode.com/problems/reverse-linked-list">Reverse Linked List</a>
 * <hr />
 *
 */
public class ReverseLinkedList {

    private ReverseLinkedList() {}

    public static class ListNode {
        int val;
        ListNode next;
        ListNode(int x) { val = x; }
    }

    public static ListNode reverseList(ListNode head) {
        ListNode dumpHead = null;
        while (head != null) {
            ListNode next = head.next;
            head.next = dumpHead;
            dumpHead = head;
            head = next;
        }
        return dumpHead;
    }

}
