package com.xsj.leetcode.easy;


public class NumberOf1Bits {

    private NumberOf1Bits() {}

    public static int hammingWeight(int n) {
        int count = 0;
        while (n != 0) {
            n &= (n - 1);   // 去掉低位的 1
            count++;
        }
        return count;
    }


}
