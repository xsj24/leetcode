package com.xsj.leetcode.easy;

import com.xsj.leetcode.util.TreeNode;

/**
 * <a href="https://leetcode.com/problems/symmetric-tree/">101. Symmetric Tree</a>
 * <hr />
 *
 * 对称树
 */
public class SymmetricTree {

    public static boolean isSymmetric(TreeNode root) {
        if (root == null) {
            return true;
        }
        return isSymmetric(root.getLeft(), root.getRight());
    }

    @SuppressWarnings("ConstantConditions")
    private static boolean isSymmetric(TreeNode left, TreeNode right) {
        if (left == null && right == null) {
            return true;
        }
        if (left == null && right != null) {
            return false;
        }
        if (left != null && right == null) {
            return false;
        }
        if (left.getVal() != right.getVal()) {
            return false;
        }
        return isSymmetric(left.getLeft(), right.getRight()) && isSymmetric(left.getRight(), right.getLeft());
    }




}
