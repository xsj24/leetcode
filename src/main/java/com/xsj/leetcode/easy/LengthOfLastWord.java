package com.xsj.leetcode.easy;

/**
 * <a href="https://leetcode.com/problems/length-of-last-word">58. Length of Last Word</a>
 * <hr/>
 * Given a string s consists of upper/lower-case alphabets and empty space characters ' ', return the length of last word in the string.
 * If the last word does not exist, return 0.
 */
public class LengthOfLastWord {

    private LengthOfLastWord() {}

    public static int lengthOfLastWord(String s) {
        int len = 0;
        int right = s.length() - 1;
        char[] chars = s.toCharArray();
        while (right >= 0 && chars[right] == ' ') {
            --right;
        }
        while (right >= 0 && chars[right] != ' ') {
            --right;
            ++len;
        }
        return len;
    }

}
