package com.xsj.leetcode.easy;

import java.util.HashMap;
import java.util.Map;

/**
 * <a href="https://leetcode.com/problems/word-pattern">Word Pattern</a>
 * <hr/>
 * Given a pattern and a string str, find if str follows the same pattern.
 * Here follow means a full match, such that there is a bijection between a letter in pattern and a non-empty word in str.
 * <pre>
 *     Examples:
 *     pattern = "abba", str = "dog cat cat dog" should return true.
 *     pattern = "abba", str = "dog cat cat fish" should return false.
 *     pattern = "aaaa", str = "dog cat cat dog" should return false.
 *     pattern = "abba", str = "dog dog dog dog" should return false.
 *     Notes:
 *     You may assume pattern contains only lowercase letters, and str contains lowercase letters separated by a single space.
 * </pre>
 */
public class WordPattern {

    // 双向映射
    public static boolean wordPattern(String pattern, String str) {
        String[] arr = str.split(" ");
        Map<Character, String> map = new HashMap<>();
        final int len = arr.length;
        if (pattern.length() != len) {
            return false;
        }
        for (int i = 0; i < len; i++) {
            char c = pattern.charAt(i);
            if (map.containsKey(c)) {
                if (!arr[i].equals(map.get(c))) {
                    return false;
                }
            } else {
                if (map.containsValue(arr[i])) {
                    return false;
                }
                map.put(c, arr[i]);
            }
        }
        return true;
    }

}
