package com.xsj.leetcode.easy;

/**
 * <a href="https://leetcode.com/problems/number-complement">number complement</a>
 * <hr/>
 * Given a positive integer, output its complement number. The complement strategy is to flip the bits of its binary representation.
 * <pre>
 * Input: 5
 * Output: 2
 * Explanation: The binary representation of 5 is 101 (no leading zero bits), and its complement is 010. So you need to output 2.
 * </pre>
 */
public class NumberComplement {

    private NumberComplement() {}

    public static int findComplement(int num) {
        int mask = 0xffffffff;
        while ( (mask & num) != 0) {
            mask <<= 1;
        }
        return ~num & ~mask;
    }

}
