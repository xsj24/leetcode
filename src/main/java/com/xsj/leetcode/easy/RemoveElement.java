package com.xsj.leetcode.easy;

/**
 * <a href="https://leetcode.com/problems/remove-element/description/">Remove Element</a>
 * Date: 2018/1/25
 * Time: 2:20
 */
public class RemoveElement {

    public static int removeElement(int[] nums, int val) {
        int i = 0;
        for (int j = 0; j < nums.length; ++j) {
            if (val != nums[j]) {
                nums[i++] = nums[j];
            }
        }
        return i;
    }

}
