package com.xsj.leetcode.easy;

/**
 * <a href="https://leetcode.com/problems/palindrome-number/" >9. Palindrome Number</a>
 */
public class PalindromeNumber {


    public static boolean isPalindrome(int x) {
        if (x < 0) {
            return false;
        }
        // 112 -> 211
        int num = 0;

        int tmp = x;
        while (tmp != 0) {
            num = num * 10 + tmp % 10;
            tmp /= 10;
        }

        return num == x;
    }


    public static boolean isPalindromeUseToString(int x) {
        if (x < 0) {
            return false;
        }
        String str = String.valueOf(x);

        int start = 0;
        int end = str.length() - 1;
        while (start < end) {
            if (str.charAt(start++) != str.charAt(end--)) {
                return false;
            }
        }
        return true;
    }

}
