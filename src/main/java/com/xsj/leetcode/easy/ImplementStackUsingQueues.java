package com.xsj.leetcode.easy;

import java.util.LinkedList;
import java.util.Queue;


/**
 * <a href="https://leetcode.com/problems/implement-stack-using-queues">225. Implement Stack Using Queues</a>
 * <hr/>
 * Implement the following operations of a stack using queues.
 * <pre>
 *     push(x) -- Push element x onto stack.
 *     pop() -- Removes the element on top of the stack.
 *     top() -- Get the top element.
 *     empty() -- Return whether the stack is empty.
 * </pre>
 */
public class ImplementStackUsingQueues {

    private ImplementStackUsingQueues() {}

    public static class MyStack {

        private Queue<Integer> queue;

        public MyStack() {
            queue = new LinkedList<>();
        }

        public void push(int x) {
            int len = queue.size();
            queue.offer(x);
            for (int i = 0; i < len; ++i) {
                queue.offer(queue.poll());
            }
        }

        public int pop() {
            return queue.poll();
        }

        public int top() {
            return queue.peek();
        }

        public boolean empty() {
            return queue.isEmpty();
        }

    }

}
