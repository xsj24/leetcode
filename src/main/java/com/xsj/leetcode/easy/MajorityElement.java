package com.xsj.leetcode.easy;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * <a href="https://leetcode.com/problems/majority-element/" >Majority Element</a>
 * 一定存在出现次数大于 n / 2 的元素
 */
public class MajorityElement {


    public static int majorityElementUsingMap(int[] nums) {
        Map<Integer, Integer> map = new HashMap<>(nums.length);

        for (int num : nums) {
            Integer count = map.get(num);
            if (count == null) {
                count = 1;
            } else {
                count += 1;
            }
            if (count > nums.length / 2) {
                return num;
            }
            map.put(num, count);
        }
        return Integer.MIN_VALUE;
    }

    public static int majorityElementRandomly(int[] nums) {
        // 随机取一个值进行判断
        Random random = new Random();
        int length = nums.length;
        while (true) {
            int majority = nums[random.nextInt(length)];
            int count = 0;
            for (int num : nums) {
                if (num == majority && ++count > length / 2) {
                    return majority;
                }
            }
        }
    }


    // O(n) 空间复杂度 O(1)
    public static int majorityElement(int[] nums) {
        int majority = nums[0];
        int count = 1;
        for (int num : nums) {
            if (num == majority) {
                ++count;
            } else {
                --count;
            }

            // 越界了，重新计数
            if (count == 0) {
                majority = num;
                count = 1;
            }
        }
        return majority;
    }


    // 中位数
    public static int majorityElementUsingSort(int[] nums) {
        Arrays.sort(nums);
        return nums[nums.length / 2];
    }

}
