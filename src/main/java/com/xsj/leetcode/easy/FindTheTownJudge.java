package com.xsj.leetcode.easy;

/**
 * <a href="https://leetcode.com/problems/find-the-town-judge/" >997. Find the Town Judge</a>
 */
public class FindTheTownJudge {

    public static int findJudge(int N, int[][] trust) {
        int[] count = new int[N + 1];
        for (int[] row : trust) {
            --count[row[0]];
            ++count[row[1]];
        }
        for (int i = 1; i <= N; ++i) {
            if (count[i] == N - 1) {
                return i;
            }
        }
        return -1;
    }



}
