package com.xsj.leetcode.easy;

/**
 * <a href="https://leetcode.com/problems/nth-digit">Nth Digit</a>
 * <hr/>
 * Find the nth digit of the infinite integer sequence 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, ... <br/>
 * <code>n</code> is positive and will fit within the range of a 32-bit signed integer (n < 231).
 */
public class NthDigit {

    private NthDigit() {}

    public static int findNthDigit(int n) {
        if (n <= 0) {
            throw new IllegalArgumentException("n = " + n);
        }
        int first = 1;
        int len = 1;
        long count = 9;
        while (n > len * count) {
            n -= count * len++;
            count *= 10;
            first *= 10;
        }
        first += (n - 1) / len;
        return String.valueOf(first).charAt((n - 1) % len) - '0';
    }

}
