package com.xsj.leetcode.easy;

import java.util.HashMap;
import java.util.Map;

/**
 * <a href="https://leetcode.com/problems/valid-anagram/" >242. Valid Anagram</a>
 */
public class ValidAnagram {

    // 最直接的方法：字节数组排序，然后进行比较

    public static boolean isAnagram(String s, String t) {
        if (s.length() != t.length()) {
            return false;
        }
        int[] count = new int[26];

        for (char c : s.toCharArray()) {
            int num = c - 'a';
            count[num] += 1;
        }
        for (char c : t.toCharArray()) {
            int num = c - 'a';
            count[num] -= 1;
        }

        for (int i : count) {
            if (i != 0) {
                return false;
            }
        }
        return true;
    }

}
