package com.xsj.leetcode.easy;

/**
 * <a href="https://leetcode.com/problems/move-zeroes/">283. Move Zeroes</a>.
 * 给定一个数组 nums ，编写一个函数将所有 0 移动到数组的末尾，同时保持非零元素的相对顺序。
 */
public class MoveZeroes {


    public static void moveZeros(int[] nums) {
        int len = nums.length;
        if (len <= 1) {
            return;
        }
        int i = 0;

        for (int j = 0; j < len; j++) {
            int num = nums[j];
            if (num != 0) {
                nums[i++] = num;
            }
        }
        for (; i < len; ++i) {
            nums[i] = 0;
        }
    }


}
