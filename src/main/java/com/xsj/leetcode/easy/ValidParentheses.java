package com.xsj.leetcode.easy;

import java.util.Stack;

/**
 * <a href="https://leetcode.com/problems/valid-parentheses">Valid Parentheses</a>
 * <hr />
 * <p>Given a string containing just the characters '(', ')', '{', '}', '[' and ']', determine if the input string is valid.</p>
 * <p>The brackets must close in the correct order, "()" and "()[]{}" are all valid but "(]" and "([)]" are not.</p>
 */
public class ValidParentheses {

    private ValidParentheses() {}

    public static boolean isValid(String s) {
        Stack<Character> stack = new Stack<>();
        for (char c : s.toCharArray()) {
            if (c == '(' || c == '[' || c == '{') {
                stack.push(c);
            } else {
                if (stack.empty()) {
                    return false;
                }
                if (c == ')' && '(' != stack.pop()) {
                    return false;
                }
                if (c == ']' && '[' != stack.pop()) {
                    return false;
                }
                if (c == '}' && '{' != stack.pop()) {
                    return false;
                }
            }
        }
        return stack.empty();
    }

    public static boolean isValid_ii(String s) {
        Stack<Character> stack = new Stack<>();
        for (char c : s.toCharArray()) {
            if (c == '(') {
                stack.push(')');
            } else if (c == '[') {
                stack.push(']');
            } else if (c == '{') {
                stack.push('}');
            } else if (stack.isEmpty() || stack.pop() != c) {
                return false;
            }
        }
        return stack.isEmpty();
    }

}
