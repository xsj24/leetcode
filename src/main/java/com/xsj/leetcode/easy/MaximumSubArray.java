package com.xsj.leetcode.easy;

/**
 * <a href="https://leetcode.com/problems/maximum-subarray/" >53. Maximum SubArray</a>
 */
public class MaximumSubArray {

    // 最大连续子序列
    public static int maxSubArray(int[] nums) {
        // [-2, 1, -3, 4, -1, 2, 1, -5, 4]
        // [-2, 1, -2, 4,  3, 5, 6,  1, 5]
        int curMax = nums[0];
        int globalMax = curMax;
        for (int i = 1; i < nums.length; i++) {
            // 重新计算 curMax
            curMax = Math.max(curMax + nums[i], nums[i]);
            globalMax = Math.max(globalMax, curMax);
        }
        return globalMax;
    }


}
