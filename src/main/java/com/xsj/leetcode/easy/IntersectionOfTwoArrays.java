package com.xsj.leetcode.easy;

import java.util.HashSet;
import java.util.Set;

/**
 * <a href="https://leetcode.com/problems/intersection-of-two-arrays/#/description">intersection-of-two-arrays</a>
 */
public class IntersectionOfTwoArrays {

    private IntersectionOfTwoArrays() {}

    // O(n)
    public static int[] intersection01(int[] nums1, int[] nums2) {
        if (nums1 == null || nums2 == null) {
            return null;
        }
        Set<Integer> set = new HashSet<>();
        Set<Integer> inter = new HashSet<>();
        for (int num : nums1) {
            set.add(num);
        }
        for (int num : nums2) {
            if (set.contains(num)) {
                inter.add(num);
            }
        }
        int[] res = new int[inter.size()];
        int i = 0;
        for (int re : inter) {
            res[i++] = re;
        }
        return res;
    }

}
