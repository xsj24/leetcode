package com.xsj.leetcode.easy;

/**
 * <a href="https://leetcode.com/problems/count-primes">Count Primes</a>
 * <hr/>
 * Count the number of prime numbers less than a non-negative number, n.
 */
public class CountPrimes {

    public static int countPrimes01(int n) {

        boolean[] isPrime = new boolean[n];
        // 2 是素数
        for (int i = 2; i < isPrime.length; i++) {
            isPrime[i] = true;
        }
        for (int i = 2; i * i < n; ++i) {
            if (!isPrime[i]) {
                continue;
            }
            for (int j = i * i; j < n; j += i) {
                isPrime[j] = false;
            }
        }
        int count = 0;
        for (int i = 2; i < n; ++i) {
            if (isPrime[i]) {
                ++count;
            }
        }
        return count;
    }

    public static int countPrimes02(int n) {
        boolean[] notPrime = new boolean[n];
        int count = 0;
        for (int i = 2; i < n; ++i) {
            if (!notPrime[i]) {
                count += 1;
                for (int j = 2; i * j < n; ++j) {
                    notPrime[i * j] = true;
                }
            }
        }
        return count;
    }

}
