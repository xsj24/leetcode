package com.xsj.leetcode.easy;

/**
 * <a href="https://leetcode.com/problems/fibonacci-number/">509. Fibonacci Number</a>
 * F(0) = 0; F(1) = 1; F(2) = 2;
 */
public class FibonacciNumber {

    public static int fib(int N) {
        if(N <= 1) {
            return N;
        }
        int pre = 0;
        int cur = 1;
        for (int i = 2; i <= N; ++i) {
            int tmp = cur + pre;
            pre = cur;
            cur = tmp;
        }
        return cur;
    }

}
