package com.xsj.leetcode.medium;

/**
 * <a href="https://leetcode.com/problems/divide-two-integers/description/">Divide Two Integers</a>
 * Date: 2018/1/25
 * Time: 2:33
 */
public class DivideTwoIntegers {

    public static int divide(int dividend, int divisor) {
        int sign = 1;
        if ((dividend > 0 && divisor < 0) || (dividend < 0 && divisor > 0)) {
            sign = -1;
        }
        long dividendL = Math.abs((long) dividend);
        long divisorL = Math.abs((long) divisor);
        if (divisorL == 0)  {
                return Integer.MAX_VALUE;
        }
        if (dividendL == 0 || (dividendL < divisorL)) {
            return 0;
        }
        long lans = divideHelper(dividendL, divisorL);
        int ans = 0;
        // Integer.MIN_VALUE / -1 溢出了
        if (lans > Integer.MAX_VALUE) {
            ans = (sign == 1) ? Integer.MAX_VALUE : Integer.MIN_VALUE;
        } else {
            ans = (int) (sign == 1 ? lans : 0 - lans);
        }
        return ans;
    }

    private static long divideHelper(long dividend, long divisor) {
        if (dividend < divisor) {
            return 0;
        }
        // Find the largest multiple so the (divisor * multiple <= dividend)
        long sum = divisor;
        long multiple = 1;
        while ((sum + sum) <= dividend) {
            sum += sum;
            multiple += multiple;
        }
        return multiple + divideHelper(dividend - sum, divisor);
    }


}
