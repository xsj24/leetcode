package com.xsj.leetcode.medium;


import com.xsj.leetcode.util.ListNode;

/**
 * <a href="https://leetcode.com/problems/partition-list/">86. Partition List<a/>
 */
public class PartitionList {


    public  static ListNode<Integer> partition(ListNode<Integer> head, int x) {

        final ListNode<Integer> h1 = new ListNode<>(-1);
        final ListNode<Integer> h2 = new ListNode<>(-1);

        ListNode<Integer> p = h1;
        ListNode<Integer> q = h2;

        while (head != null) {
            if (head.getItem() < x) {
                p.setNext(head);
                p = p.getNext();
            } else {
                q.setNext(head);
                q = q.getNext();
            }
            head = head.getNext();
        }

        q.setNext(null);
        p.setNext(h2.getNext());


        return h1.getNext();
    }


}
