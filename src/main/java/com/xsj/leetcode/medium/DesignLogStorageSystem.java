package com.xsj.leetcode.medium;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <a href="https://leetcode.com/problems/design-log-storage-system">635. Design Log Storage System</a>
 * <hr/>
 * <pre>
 * You are given several logs that each log contains a unique id and timestamp. Timestamp is a string that has the following format:
 * Year:Month:Day:Hour:Minute:Second, for example, 2017:01:01:23:59:59. All domains are zero-padded decimal numbers.
 * Design a log storage system to implement the following functions:
 * void Put(int id, string timestamp): Given a log's unique id and timestamp, store the log in your storage system.
 * int[] Retrieve(String start, String end, String granularity): Return the id of logs whose timestamps are within the range from start to end.
 * Start and end all have the same format as timestamp. However, granularity means the time level for consideration.
 * For example, start = "2017:01:01:23:59:59", end = "2017:01:02:23:59:59", granularity = "Day",
 * it means that we need to find the logs within the range from Jan. 1st 2017 to Jan. 2nd 2017.
 *
 * Example:
 * put(1, "2017:01:01:23:59:59");
 * put(2, "2017:01:01:22:59:59");
 * put(3, "2016:01:01:00:00:00");
 * retrieve("2016:01:01:01:01:01","2017:01:01:23:00:00","Year");
 *      return [1,2,3], because you need to return all logs within 2016 and 2017.
 * retrieve("2016:01:01:01:01:01","2017:01:01:23:00:00","Hour");
 *      return [1,2], because you need to return all logs start from 2016:01:01:01 to 2017:01:01:23, where log 3 is left outside the range.
 * </pre>
 */
public class DesignLogStorageSystem {

    public static class LogSystem {

        Map<Integer,String> map;

        public enum Index {
            Year(4), Month(7), Day(10), Hour(13), Minute(16), Second(19);
            int idx;   Index(int i) {this.idx = i;}
        }

        public LogSystem() {
            map = new HashMap<>();
        }

        public void put(int id, String timestamp) {
            map.put(id, timestamp);
        }

        public List<Integer> retrieve(String s, String e, final String gra) {
            List<Integer> res = new ArrayList<>();
            int end = Index.valueOf(gra).idx;
            for (Map.Entry<Integer, String> entry : map.entrySet()) {
                String substring = entry.getValue().substring(0, end);
                if (substring.compareTo(s.substring(0, end)) >= 0
                        && substring.compareTo(e.substring(0, end)) <= 0) {
                    res.add(entry.getKey());
                }
            }
            return res;
        }
    }


}
