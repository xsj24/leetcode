package com.xsj.leetcode.medium;

import com.xsj.leetcode.util.TreeNode;

/**
 * <a href="https://leetcode.com/problems/sum-root-to-leaf-numbers">Sum Root to Leaf Numbers</a>
 * <hr/>
 * Given a binary tree containing digits from 0-9 only, each root-to-leaf path could represent a number.
 * An example is the root-to-leaf path 1->2->3 which represents the number 123.
 * Find the total sum of all root-to-leaf numbers.
 * For example,
 * <pre>
 *         1
 *        / \
 *       2   3
 * The root-to-leaf path 1->2 represents the number 12.
 * The root-to-leaf path 1->3 represents the number 13.
 * Return the sum = 12 + 13 = 25.
 * </pre>
 */
public class SumRootToLeafNumbers {

    public static int sumNumbers(TreeNode root) {
        return sum(root, 0);
    }

    private static int sum(TreeNode root, int sum) {
        if (root == null) {
            return 0;
        }
        if (root.getLeft() == null && root.getRight() == null) {
            return sum * 10 + root.getVal();
        }
        return sum(root.getLeft(), sum * 10 + root.getVal()) + sum(root.getRight(), sum * 10 + root.getVal());
    }




}
