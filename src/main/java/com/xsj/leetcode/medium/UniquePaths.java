package com.xsj.leetcode.medium;

/**
 * <a href="https://leetcode.com/problems/unique-paths/">Unique Paths</a>.
 * <hr/>
 * The robot can only move either down or right at any point in time.
 */
public class UniquePaths {

    private UniquePaths() {}

    public static int uniquePaths(int m, int n) {
        if (m == 0 || n == 0) {
            return 0;
        }
        int[] states = new int[n];
        for (int i = 0; i < n; i++) {
            states[i] = 1;
        }
        for (int i = 0; i < m - 1; ++i) {
            for (int j = 1; j < n; ++j) {
                states[j] = states[j - 1] + states[j];
            }
        }
        return states[n - 1];
    }

}
