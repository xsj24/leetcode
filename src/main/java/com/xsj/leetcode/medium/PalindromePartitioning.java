package com.xsj.leetcode.medium;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * <a href="https://leetcode.com/problems/palindrome-partitioning/">Palindrome Partitioning</a>
 *
 */
public abstract class PalindromePartitioning {

    private PalindromePartitioning() { }

    public static List<List<String>>  partition(String s) {
        if (s == null || s.isEmpty()) {
            return Collections.emptyList();
        }
        List<List<String>> result = new ArrayList<>();
        List<String> temp = new ArrayList<>();
        backtrace(result, temp, s, 0);
        return result;
    }

    private static void backtrace(List<List<String>> result, List<String> temp, String s, int start) {
        if (start >= s.length()) {
            result.add(new ArrayList<>(temp));
        } else {
            for (int i = start; i < s.length(); i++) {
                if (isPalindrome(s, start, i)) {
                    temp.add(s.substring(start, i + 1));
                    backtrace(result, temp, s, i + 1);
                    temp.remove(temp.size() - 1);
                }
            }
        }
    }

    private static boolean isPalindrome(String str, int start, int end) {
        while (start < end && str.charAt(start) == str.charAt(end)) {
            ++start;
            --end;
        }
        return start >= end;
    }

}
