package com.xsj.leetcode.medium;

import java.util.Stack;

/**
 * <a href="https://leetcode.com/problems/simplify-path">Simplify Path</a>.
 * Given an absolute path for a file (Unix-style), simplify it.
 *
 * <pre>
 * For example,
 * path = "/home/", => "/home"
 * path = "/a/./b/../../c/", => "/c"
 * </pre>
 */
public class SimplifyPath {

    private SimplifyPath() {}

    public static String simplifyPath(String path) {
        if (path == null || path.length() == 0) {
            return path;
        }
        String[] split = path.split("/");
        Stack<String> stack = new Stack<>();
        for (String p : split) {
            if (p.equals("..")) {
                if (!stack.empty()) {
                    stack.pop();
                }
            } else if (!p.equals(".") && !p.isEmpty()) {  // 空串不要入栈
                stack.push(p);
            }
        }
        if (stack.empty()) {
            return "/";
        }
        StringBuilder builder = new StringBuilder();
        while (!stack.empty()) {
            builder.insert(0, stack.pop());
            builder.insert(0, "/");

        }
        return builder.toString();
    }

}
