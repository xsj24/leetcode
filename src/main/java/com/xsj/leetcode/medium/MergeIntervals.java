package com.xsj.leetcode.medium;

import java.util.*;

/**
 * <a href="https://leetcode.com/problems/merge-intervals">Merge Intervals<a/>
 * <hr/>
 *
 * merge all overlapping intervals
 * for example,
 * <pre>
 *     [1, 3], [2, 6], [8, 10], [15, 18]
 *     [1, 6], [8, 10], [15, 18]
 * </pre>
 *
 */
public class MergeIntervals {

    public static class Interval {
        public int start;
        public int end;

        public Interval() {
            this(0, 0);
        }

        public Interval(int start, int end) {
            this.start = start;
            this.end = end;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Interval interval = (Interval) o;

            if (start != interval.start) return false;
            return end == interval.end;

        }

        @Override
        public int hashCode() {
            int result = start;
            result = 31 * result + end;
            return result;
        }
    }

    public static List<Interval> merge01(List<Interval> intervals) {
        Collections.sort(intervals, new IntervalComparator());
        List<Interval> res = new ArrayList<>(intervals.size());
        Interval pre = intervals.get(0);
        // 排序后迭代
        for (int i = 1; i < intervals.size(); i++) {
            Interval cur = intervals.get(i);
            if (cur.start <= pre.end) {
                pre = new Interval(pre.start, Math.max(pre.end, cur.end));
            } else {
                res.add(pre);
                pre = cur;
            }
        }
        res.add(pre);
        return res;
    }

    static class IntervalComparator implements Comparator<Interval> {
        @Override
        public int compare(Interval o1, Interval o2) {
            return o1.start - o2.start;
        }
    }

    public static List<Interval> merge02(List<Interval> intervals) {
        int n = intervals.size();
        int[] starts = new int[n];
        int[] ends = new int[n];
        for (int i = 0; i < n; i++) {
            Interval interval = intervals.get(i);
            starts[i] = interval.start;
            ends[i] = interval.end;
        }
        Arrays.sort(starts);
        Arrays.sort(ends);
        // starts: 1, 2, 8, 15
        // ends: 3, 6, 10, 18
        List<Interval> res = new ArrayList<>(intervals.size());
        for (int i = 0, j = 0; i < n; i++) { // j is start of interval.
            //  the latter one's start must > previous one's end.
            if (i == n - 1 || starts[i + 1] > ends[i]) {
                res.add(new Interval(starts[j], ends[i]));
                j = i + 1;
            }
        }
        return res;

    }

}
