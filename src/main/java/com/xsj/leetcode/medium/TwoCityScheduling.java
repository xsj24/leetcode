package com.xsj.leetcode.medium;


import java.util.Arrays;

/**
 * 两城调度
 * <a href="https://leetcode.com/problems/two-city-scheduling/">1029. Two City Scheduling</a>.
 *
 * A company is planning to interview 2n people. Given the array costs where costs[i] = [aCosti, bCosti],
 * the cost of flying the ith person to city a is aCosti, and the cost of flying the ith person to city b is bCosti.
 */
public class TwoCityScheduling {


    public static int twoCitySchedCost(int[][] costs) {
        int len = costs.length;

        int n = len >> 1;

        int[] refunds = new int[len];
        int minCost = 0;

        int i = 0;
        for (int[] arr : costs) {
            minCost += arr[0];
            refunds[i++] = arr[1] - arr[0];
        }

        Arrays.sort(refunds);
        for (int j = 0; j < n; j++) {
            minCost += refunds[j];
        }
        return minCost;
    }




}
