package com.xsj.leetcode.medium;

import java.util.*;

/**
 * <a href="https://leetcode.com/problems/repeated-dna-sequences/" >187. Repeated DNA Sequences</a>
        */
public class RepeatedDnsSequences {

    public static List<String> findRepeatedDnaSequences(String s) {
        int length = s.length();
        if (length <= 10) {
            return Collections.emptyList();
        }
        Map<String, Integer> map = new HashMap<>(length - 9);
        ArrayList<String> res = new ArrayList<>(length - 9);
        for (int i = 0; i < length - 9; ++i) {
            String seq = s.substring(i, i + 10);
            Integer value = map.get(seq);
            int count = value == null ? 0 : value;
            if (count == 1) {
                res.add(seq);
            }
            map.put(seq, ++count);
        }
        return res;
    }


}
