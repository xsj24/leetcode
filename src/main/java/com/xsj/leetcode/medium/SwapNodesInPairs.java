package com.xsj.leetcode.medium;

/**
 * <a href="https://leetcode.com/problems/swap-nodes-in-pairs">24. Swap Nodes in Pairs</a>
 * <hr/>
 * <p>Given a linked list, swap every two adjacent nodes and return its head.</p>
 * For example,
 * Given 1->2->3->4, you should return the list as 2->1->4->3.
 * <p>Your algorithm should use only constant space. You may <strong>not</strong> modify the values in the list, only nodes itself can be changed.</p>
 */
public class SwapNodesInPairs {

    private SwapNodesInPairs() {}

    public static class ListNode {
        int val;
        ListNode next;
        ListNode(int val) {this.val = val;}
    }

    public static ListNode swapPairs(ListNode head) {
        ListNode dummyHead = new ListNode(-1);
        dummyHead.next = head;
        ListNode cur = dummyHead;
        while (cur.next != null && cur.next.next != null) {
            ListNode first = cur.next;
            ListNode second = first.next;
            first.next = second.next;
            cur.next = second;
            second.next = first;
            cur = cur.next.next;
        }
        return dummyHead.next;
    }


}
