package com.xsj.leetcode.medium;

import java.util.HashMap;
import java.util.Map;

/**
 * <a href="https://leetcode.com/problems/decode-ways/" >91. Decode Ways</a>
 */
public class DecodeWays {

    public static int numDecodings(String s) {
        if (s.length() == 0) {
            return 0;
        }
        Map<String, Integer> maps = new HashMap<>(s.length());
        maps.put("", 1);
        return ways(s,  maps);
    }

    private static int ways(String s, Map<String, Integer> maps) {
        if (maps.containsKey(s)) {
            return maps.get(s);
        }
        if (s.charAt(0) == '0') {
            return 0;
        }
        if (s.length() == 1) {
            return 1;
        }
        int w = ways(s.substring(1), maps);
        int prefix = (s.charAt(0) - '0') * 10 + (s.charAt(1) - '0');
        if (prefix <= 26) {
            w += ways(s.substring(2), maps);
        }
        maps.put(s, w);
        return w;
    }

}
