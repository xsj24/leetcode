package com.xsj.leetcode.medium;

/**
 * <a href="https://leetcode.com/problems/counting-bits">Counting Bits</a>
 * <hr/>
 * Given a non negative integer number num. For every numbers i in the range 0 ≤ i ≤ num calculate
 * the number of 1's in their binary representation and return them as an array.
 * Example:
 * <pre>
 *     For num = 5 you should return [0,1,1,2,1,2].
 * </pre>
 */
public class CountingBits {


    public static int[] countBits01(int num) {
        int[] res = new int[num + 1];
        for (int i = 0; i <= num; ++i) {
            int count = 0;
            int tmp = i;
            while (tmp != 0) {
                count += 1;
                tmp = tmp & (tmp - 1);
            }
            res[i] = count;
        }
        return res;
    }

    public static int[] countBits02(int num) {
        int[] res = new int[num + 1];
        for (int i = 1; i <= num; ++i)
            res[i] = res[i & (i - 1)] + 1;
        return res;
    }

}
