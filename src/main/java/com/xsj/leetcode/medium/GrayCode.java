package com.xsj.leetcode.medium;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * <a href="https://leetcode.com/problems/gray-code/">89. Gray Code</a>.
 */
public class GrayCode {

    // 采用数学公式
    public static List<Integer> grayCode(int n) {
        if (n == 0) {
            return Collections.singletonList(0);
        }
        int size = 1 << n;
        List<Integer> result = new ArrayList<>(size);
        for (int i = 0; i < size; i++) {
            result.add(binary2Gray(i));
        }
        return result;
    }

    private static int binary2Gray(int i) {
        return i ^ (i >> 1);
    }


    /**
     * n = 1 | n = 2 | n = 3
     * 0     | 00    | 000
     * 1     | 01    | 001
     *       | 11    | 011
     *       | 10    | 010
     *               | 110
     *               | 111
     *               | 101
     *               | 100
     */
    public static List<Integer> grayCodeI(int n) {
        if (n == 0) {
            return Collections.singletonList(0);
        }
        List<Integer> result = new ArrayList<>(1 << n);
        result.add(0);

        for (int i = 0; i < n; i++) {
            int highBit = 1 << i;
            for (int j = result.size() - 1; j >= 0; --j) {
                result.add(highBit | result.get(j));  // 反向遍历
            }
        }

        return result;
    }


}
