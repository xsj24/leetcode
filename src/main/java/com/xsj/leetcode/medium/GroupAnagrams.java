package com.xsj.leetcode.medium;

import java.util.*;

/**
 * <a href="https://leetcode.com/problems/group-anagrams/" >49. Group Anagrams</a>
 */
public class GroupAnagrams {


    public static List<List<String>> groupAnagrams(String[] strs) {
        if (strs.length == 0) {
            return Collections.emptyList();
        }

        // All inputs will be in lowercase.  key 排序可进行优化
        Map<String, List<String>> helper = new HashMap<>(strs.length);
        for (String str : strs) {
            char[] chars = str.toCharArray();
            Arrays.sort(chars);
            String tmp = String.valueOf(chars);
            if (helper.containsKey(tmp)) {
                helper.get(tmp).add(str);
            } else {
                ArrayList<String> list = new ArrayList<>();
                list.add(str);
                helper.put(tmp, list);
            }
        }

        List<List<String>> result = new ArrayList<>(helper.size());
        for (List<String> list : helper.values()) {
            result.add(list);
        }
        return result;
    }

}
