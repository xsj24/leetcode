package com.xsj.leetcode.medium;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * <a href="https://leetcode.com/problems/lru-cache/">146. LRU Cache</a>.
 */
class LRUCache {

    public interface Cacheable {

        int get(int key);

        void put(int key, int value);

        int size();

        int capacity();

    }


    // 基于 JDK LinkedHashMap 简单实现
    public static class LRUCache01 extends LinkedHashMap<Integer, Integer>
            implements Cacheable {

        private final int capacity;

        public LRUCache01(int capacity) {
            super(capacity, 0.75F, true);
            this.capacity = capacity;
        }

        public int get(int key) {
            return super.getOrDefault(key, -1);
        }

        public void put(int key, int value) {
            super.put(key, value);
        }

        @Override
        protected boolean removeEldestEntry(Map.Entry<Integer, Integer> eldest) {
            return size() > this.capacity;
        }

        @Override
        public int capacity() {
            return capacity;
        }
    }

    public static class LRUCache02 implements Cacheable {

        private final Map<Integer, Node> map;
        private final int capacity;
        private int count = 0;
        private final Node head;
        private final Node tail;

        public LRUCache02(int capacity) {
            this.capacity = capacity;
            this.map = new HashMap<>(capacity);
            this.head = new Node(-1, -1);
            this.tail = new Node(-1, -1);
            head.next = tail;
            tail.pre = head;
        }

        @Override
        public int size() {
            return count;
        }

        @Override
        public int capacity() {
            return capacity;
        }

        @Override
        public int get(int key) {
            Node node = map.get(key);
            if (node == null) {
                return -1;
            }
            int result = node.value;
            deleteNode(node);
            addToHead(node);
            return result;
        }

        @Override
        public void put(int key, int value) {
            Node node = map.get(key);
            if (node != null) {
                node.value = value;
                deleteNode(node);
                addToHead(node);
            } else {
                Node newNode = new Node(key, value);
                map.put(key, newNode);
                if (this.count < capacity) {
                    ++count;
                } else {
                    map.remove(tail.pre.key);
                    deleteNode(tail.pre);
                }
                addToHead(newNode);
            }
        }

        private static void deleteNode(Node node) {
            node.pre.next = node.next;
            node.next.pre = node.pre;
        }

        private void addToHead(Node node) {
            node.next = head.next;
            node.next.pre = node;
            node.pre = head;
            head.next = node;
        }


        static class Node {
            int key;
            int value;
            Node pre, next;
            Node(int key, int value) {
                this.key = key;
                this.value = value;
            }
        }
    }


}
