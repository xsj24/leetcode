package com.xsj.leetcode.medium;

/**
 * <a href="https://leetcode.com/problems/unique-paths-ii">Unique Paths II</a>.
 * <hr/>
 * [
 *  [0,0,0],
 *  [0,1,0],
 *  [0,0,0]
 * ]
 * 1 表示实心块
 */
public class UniquePathsII {

    private UniquePathsII() {}

    public static int uniquePathsWithObstacles(int[][] obstacleGrid) {
        int row = obstacleGrid.length;
        int col = obstacleGrid[0].length;
        int[] states = new int[col];
        states[0] = obstacleGrid[0][0] == 1 ? 0 : 1;
        for (int i = 0; i < row; ++i) {
            for (int j = 0; j < col; j++) {
                if (obstacleGrid[i][j] == 1) {
                    states[j] = 0;
                } else if (j > 0) {
                    states[j] += states[j - 1];
                }
            }
        }
        return states[col - 1];
    }

}
