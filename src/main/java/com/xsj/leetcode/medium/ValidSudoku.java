package com.xsj.leetcode.medium;

/**
 * <a href="https://leetcode.com/problems/valid-sudoku/" >36. Valid Sudoku</a>
 */
public class ValidSudoku {


    public static boolean isValidSudoku(char[][] board) {
        int len = board.length;
        boolean[] isExist = new boolean[10];

        // 行判断
        for (int i = 0; i < len; i++) {
            for (int j = 0; j < len; j++) {
                char c = board[i][j];
                if (c == '.') {
                    continue;
                }
                int num = c - '0';
                if (isExist[num]) {
                    return false;
                } else {
                    isExist[num] = true;
                }
            }
            for (int j = 1; j < isExist.length; j++) {
                isExist[j] = false;
            }
        }

        // 列判断
        for (int i = 0; i < len; i++) {
            for (int j = 0; j < len; j++) {
                char c = board[j][i];
                if (c == '.') {
                    continue;
                }
                int num = c - '0';
                if (isExist[num]) {
                    return false;
                } else {
                    isExist[num] = true;
                }
            }
            for (int j = 1; j < isExist.length; j++) {
                isExist[j] = false;
            }
        }


        /**
         * 3 * 3 块判断
         *
         * (0,0)  (0,3)  (0,6)
         * (3,0)  (3,3)  (3,6)
         * (6,0)  (6,3)  (6,6)
          */
        for (int i = 0; i < 9; i++) {

            int row = 3 * (i / 3);
            int col = 3 * (i % 3);

            for (int j = row; j < row + 3; ++j) {
                for (int k = col; k < col + 3; ++k) {
                    char c = board[j][k];
                    if (c == '.') {
                        continue;
                    }
                    int num = c - '0';
                    if (isExist[num]) {
                        return false;
                    } else {
                        isExist[num] = true;
                    }
                }
            }
            for (int j = 1; j < isExist.length; j++) {
                isExist[j] = false;
            }
        }
        return true;
    }
}
