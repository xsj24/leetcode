package com.xsj.leetcode.medium;

/**
 * <a href="https://leetcode.com/problems/sort-colors">75. Sort Colors</a>
 * <hr/>
 * <p>Given an array with n objects colored red, white or blue, sort them so that objects of the same color are adjacent, with the colors in the order red, white and blue.</p>
 * <p>Here, we will use the integers 0, 1, and 2 to represent the color red, white, and blue respectively.</p>
 * 三色旗
 */
public class SortColors {

    private SortColors() {}

    public static void sortColors(int[] nums) {
        int l = 0;
        int r = nums.length - 1;
        for (int i = 0; i < r; i++) {
            if (nums[i] == 0) {
                int tmp = nums[i];
                nums[i] = nums[l];
                nums[l] = tmp;
                ++l;
            } else if (nums[i] == 2) {
                int tmp = nums[i];
                nums[i] = nums[r];
                nums[r] = tmp;
                --i;
                --r;
            }
        }
    }

}
