package com.xsj.leetcode.hard;

import java.util.Stack;

/**
 * <a href="https://leetcode.com/problems/longest-valid-parentheses">longest valid parentheses</a>
 * <hr/>
 * Given a string containing just the characters '(' and ')',
 * find the length of the longest valid (well-formed) parentheses substring.
 * <p/>
 * For "(()", the longest valid parentheses substring is "()", which has length = 2.
 * Another example is ")()())", where the longest valid parentheses substring is "()()", which has length = 4.
 *
 */
public class LongestValidParentheses {

    private LongestValidParentheses() {}

    // 使用栈 TODO 还是一脸懵逼
    public static int longestValidParentheses(String s) {
        int left = -1;
        int max = 0;
        Stack<Integer> stack = new Stack<>();
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '(') {
                stack.push(i);
            } else {
                if (stack.isEmpty()) {
                    left = i;
                } else {
                    stack.pop();  // match
                    if (stack.isEmpty()) {
                        max = Math.max(max, i - left);
                    } else {
                        max = Math.max(max, i - stack.peek());
                    }
                }
            }
        }
        return max;
    }

}
