package com.xsj.leetcode.hard;

/**
 * <a href="https://leetcode.com/problems/wildcard-matching">Wildcard Matching</a>
 * <hr />
 * Implement wildcard pattern matching with support for '?' and '*'.
 * <pre>
 *   '?' Matches any single character.
 *   '*' Matches any sequence of characters (including the empty sequence).
 *
 *   The matching should cover the entire input string (not partial).
 *
 *   The function prototype should be:
 *   bool isMatch(const char *s, const char *p)
 *
 *   Some examples:
 *   isMatch("aa","a") → false
 *   isMatch("aa","aa") → true
 *   isMatch("aaa","aa") → false
 *   isMatch("aa", "*") → true
 *   isMatch("aa", "a*") → true
 *   isMatch("ab", "?*") → true
 *   isMatch("aab", "c*a*b") → false
 * </pre>
 */
public class WildcardMatching {


    public static boolean isMatch(String s, String p) {



        return false;
    }

}
