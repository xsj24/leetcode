package com.xsj.leetcode.hard;

import java.util.ArrayList;
import java.util.List;

/**
 * <a href="https://leetcode.com/problems/insert-interval/">Insert Interval</a>
 * <hr/>
 * Given a set of non-overlapping intervals, insert a new interval into the intervals (merge if necessary).
 * You may assume that the intervals were initially sorted according to their start times.
 * <pre>
 *     Given [1,2],[3,5],[6,7],[8,10],[12,16], insert and merge [4,9] in as [1,2],[3,10],[12,16].
 * </pre>
 */
public class InsertInterval {

    public static class Interval {
        public int start;
        public int end;

        public Interval() {
            this(0, 0);
        }

        public Interval(int start, int end) {
            this.start = start;
            this.end = end;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Interval interval = (Interval) o;

            if (start != interval.start) return false;
            return end == interval.end;

        }

        @Override
        public int hashCode() {
            int result = start;
            result = 31 * result + end;
            return result;
        }
    }

    // // TODO: 2017/11/6  
    public static List<Interval> insert(List<Interval> intervals, Interval newInterval) {
        List<Interval> res = new ArrayList<>();
        for (Interval i : intervals) {
            if (newInterval == null || i.end < newInterval.start) {
                res.add(i);
            } else if (i.start > newInterval.end) {
                
            }
        }
        return null;
    }

}
