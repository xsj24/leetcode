package com.xsj.leetcode.util;

public abstract class ListHelper {

    private ListHelper() {}

    /**
     * 单链表创建
     */
    public static <T> ListNode<T> create(T... values) {
        if (values == null || values.length == 0) {
            throw new IllegalArgumentException("illegal values");
        }
        final ListNode<T> dumpHeader = new ListNode<>(values[0]);
        ListNode<T> p = dumpHeader;
        for (T value : values) {
            p.setNext(new ListNode<>(value));
            p = p.getNext();
        }
        return dumpHeader.getNext();
    }

    public static void print(ListNode<?> head) {
        if (head == null) {
            throw new IllegalArgumentException("head required");
        }
        ListNode<?> p = head;
        while (p != null) {
            System.out.println(p);
            p = p.getNext();
        }
    }

    public static <T> boolean equalTo(ListNode<T> l, ListNode<T> r) {
        if (l == null || r == null) {
            throw new IllegalArgumentException("args required");
        }
        while (l != null && r != null) {
            if (!l.getItem().equals(r.getItem())) {
                return false;
            }
            l = l.getNext();
            r = r.getNext();
        }
        return l == null && r == null;
    }

}
