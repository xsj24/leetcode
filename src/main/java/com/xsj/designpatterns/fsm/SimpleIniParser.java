package com.xsj.designpatterns.fsm;

public class SimpleIniParser {

    enum State {
        NONE, GROUP, KEY_VALUE, COMMENT
    }

    public static void parse(String str) {
        State state = State.NONE;

        int groupStart = 0;
        int groupEnd;

        int commentStart = 0;
        int commentEnd;

        int kvStart = 0;
        int kvEnd;

        char[] chars = str.toCharArray();

        for (int i = 0; i < chars.length; i++) {
            char cur = chars[i];

            switch (state) {
                case NONE:
                    if (cur == '[') {
                        state = State.GROUP;
                        groupStart = i + 1;
                    } else if (cur == ';') {
                        state = State.COMMENT;
                        commentStart = i + 1;
                    }  else if (!Character.isWhitespace(cur)) {
                        state = State.KEY_VALUE;
                        kvStart = i;
                    }
                    break;
                case GROUP:
                    if (cur == ']') {
                        state = State.NONE;
                        groupEnd = i;
                        System.out.printf("group = %s\n", str.substring(groupStart, groupEnd));
                    } else if (cur == '\r' || cur == '\n') {
                        state = State.NONE;
                    }
                    break;
                case COMMENT:
                    if (cur == '\r' || cur == '\n') {
                        state = State.NONE;
                        commentEnd = i;
                        System.out.printf("comment = %s\n", str.substring(commentStart, commentEnd));
                    }
                    break;
                case KEY_VALUE:
                    if (cur == '\r' || cur == '\n') {
                        state = State.NONE;
                        kvEnd = i;
                        String keyValue = str.substring(kvStart, kvEnd);
                        int index = keyValue.indexOf('=');
                        if (index < 0) {
                            break;
                        }
                        System.out.printf("key = %s, value = %s\n", keyValue.substring(0, index), keyValue.substring(index + 1));
                    }
            }

        }


    }

}
