package com.xsj.designpatterns.state.mammoth;

import java.util.Objects;

public class Mammoth {

    private State state;

    public Mammoth() {
        this.state = new PeacefulState(this);
    }

    public Mammoth(State state) {
        Objects.requireNonNull(state, "state required!");
        this.state = state;
    }

    /**
     * make time pass for the mammoth
     */
    public void timePasses() {
        if (state.getClass().equals(PeacefulState.class)) {
            changeStateTo(new AngryState(this));
        } else {
            changeStateTo(new PeacefulState(this));
        }
    }

    private void changeStateTo(State state) {
        this.state = state;
        this.state.onEnterState();
    }

    public void observe() {
        this.state.observe();
    }

    @Override
    public String toString() {
        return "Mammoth{" + this.hashCode() + '}';
    }
}
