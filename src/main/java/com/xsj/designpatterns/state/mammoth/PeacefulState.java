package com.xsj.designpatterns.state.mammoth;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PeacefulState implements State {

    private final static Logger LOG = LoggerFactory.getLogger(PeacefulState.class);

    private Mammoth mammoth;

    public PeacefulState(Mammoth mammoth) {
        this.mammoth = mammoth;
    }

    @Override
    public void observe() {
        LOG.info("{} is calm and peaceful.", mammoth);
    }

    @Override
    public void onEnterState() {
        LOG.info("{} calms down.", mammoth);
    }
}
