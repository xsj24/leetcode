package com.xsj.designpatterns.state.mammoth;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AngryState implements State {

    private final static Logger LOG = LoggerFactory.getLogger(AngryState.class);

    private Mammoth mammoth;

    public AngryState(Mammoth mammoth) {
        this.mammoth = mammoth;
    }

    @Override
    public void observe() {
        LOG.info("{} is furious!", mammoth);
    }

    @Override
    public void onEnterState() {
        LOG.info("{} gets angry!", mammoth);
    }

}
