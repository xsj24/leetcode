package com.xsj.designpatterns.state.mammoth;

public interface State {

    void observe();

    void onEnterState();

}
