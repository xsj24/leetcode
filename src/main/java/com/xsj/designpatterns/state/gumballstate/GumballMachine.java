package com.xsj.designpatterns.state.gumballstate;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GumballMachine {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private int count;

    private State state;

    public GumballMachine(int numberGumballs) {
        this.count = numberGumballs;
        if (count > 0) {
            state = NoQuarterState.getInstance();
        } else {
            state = SoldOutState.getInstance();
        }
    }

    void setState(State state) {
        this.state = state;
    }


    public void insertQuarter() {
        state.insertQuarter(this);
    }

    public void ejectQuarter() {
        state.ejectQuarter(this);
    }

    public void turnCrank() {
        state.turnCrank(this);
        state.dispense(this);
    }

    void releaseBall() {
        log.info("A gumball comes rolling out the slot...");
        if (count != 0) {
            count = count - 1;
        }
    }

    void refill(int count) {
        this.count += count;
        log.info("The gumball machine was just refilled; it's new count is: {}", this.getCount());
        state.refill(this);
    }

    int getCount() {
        return count;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append("\nMighty Gumball, Inc.");
        result.append("\nJava-enabled Standing Gumball Model #2004");
        result.append("\nInventory: ").append(count).append(" gumball");
        if (count != 1) {
            result.append("s");
        }
        result.append("\n");
        result.append("Machine is ").append(state).append("\n");
        return result.toString();
    }

}
