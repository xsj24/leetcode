package com.xsj.designpatterns.state.gumballstate;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NoQuarterState implements State {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private static final NoQuarterState INSTANCE = new NoQuarterState();

    public static NoQuarterState getInstance() {
        return INSTANCE;
    }

    private NoQuarterState() {}

    @Override
    public void insertQuarter(GumballMachine machine) {
        log.info("You inserted a quarter");
        machine.setState(HasQuarterState.getInstance());
    }

    @Override
    public void ejectQuarter(GumballMachine machine) {
        throw new IllegalStateException("You haven't inserted a quarter");
    }

    @Override
    public void turnCrank(GumballMachine machine) {
        throw new IllegalStateException("You turned, but there's no quarter");
    }

    @Override
    public void dispense(GumballMachine machine) {
        throw new IllegalStateException("You need to pay first");
    }

    @Override
    public void refill(GumballMachine machine) {

    }

    @Override
    public String toString() {
        return "waiting for quarter";
    }
}
