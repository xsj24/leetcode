package com.xsj.designpatterns.state.gumballstate;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HasQuarterState implements State {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private static final HasQuarterState INSTANCE = new HasQuarterState();

    public static HasQuarterState getInstance() {
        return INSTANCE;
    }

    private HasQuarterState() {
    }

    @Override
    public void insertQuarter(GumballMachine machine) {
        throw new UnsupportedOperationException("You can't insert another quarter");
    }

    @Override
    public void ejectQuarter(GumballMachine machine) {
        log.info("Quarter return");
        machine.setState(NoQuarterState.getInstance());
    }

    @Override
    public void turnCrank(GumballMachine machine) {
        log.info("You turned...");
        machine.setState(SoldState.getInstance());
    }

    @Override
    public void dispense(GumballMachine machine) {
        throw new IllegalStateException("You need to pay first");
    }

    @Override
    public void refill(GumballMachine machine) {

    }

    @Override
    public String toString() {
        return "waiting for turn of crank";
    }
}
