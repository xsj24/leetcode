package com.xsj.designpatterns.state.gumballstate;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SoldState implements State {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private static final SoldState INSTANCE = new SoldState();

    public static SoldState getInstance() {
        return INSTANCE;
    }

    private SoldState() {
    }

    @Override
    public void insertQuarter(GumballMachine machine) {
        throw new UnsupportedOperationException("Please wait, we're already giving you a gumball");
    }

    @Override
    public void ejectQuarter(GumballMachine machine) {
        throw new UnsupportedOperationException("Sorry, you already turned the crank");
    }

    @Override
    public void turnCrank(GumballMachine machine) {
        throw new UnsupportedOperationException("Turning twice doesn't get you another gumball!");
    }

    @Override
    public void dispense(GumballMachine machine) {
        machine.releaseBall();
        if (machine.getCount() > 0) {
            machine.setState(NoQuarterState.getInstance());
        } else {
            log.info("Oops, out of gumballs!");
            machine.setState(SoldOutState.getInstance());
        }

    }

    @Override
    public void refill(GumballMachine machine) {

    }

    @Override
    public String toString() {
        return "dispensing a gumball";
    }
}
