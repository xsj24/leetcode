package com.xsj.designpatterns.state.gumballstate;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SoldOutState implements State {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private static final SoldOutState INSTANCE = new SoldOutState();

    public static SoldOutState getInstance() {
        return INSTANCE;
    }

    private SoldOutState() {
    }

    @Override
    public void insertQuarter(GumballMachine machine) {
        throw new UnsupportedOperationException("You can't insert a quarter, the machine is sold out");
    }

    @Override
    public void ejectQuarter(GumballMachine machine) {
        throw new UnsupportedOperationException("You can't eject a quarter, the machine is sold out");
    }

    @Override
    public void turnCrank(GumballMachine machine) {
        log.info("You turned, but there are no gumballs");
    }

    @Override
    public void dispense(GumballMachine machine) {
        log.info("No gumball dispensed");

    }

    @Override
    public void refill(GumballMachine machine) {
        machine.setState(NoQuarterState.getInstance());
    }

    @Override
    public String toString() {
        return "sold out";
    }
}
