package com.xsj.designpatterns.state.gumballstate;

public interface State {

    void insertQuarter(GumballMachine machine);

    void ejectQuarter(GumballMachine machine);

    void turnCrank(GumballMachine machine);

    void dispense(GumballMachine machine);

    void refill(GumballMachine machine);
}