package com.xsj.designpatterns.state.gumballstate;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GumballMachineTestDrive {

    private final static Logger LOG = LoggerFactory.getLogger(GumballMachineTestDrive.class);

    public static void main(String[] args) {
        GumballMachine gumballMachine = new GumballMachine(2);

        LOG.info("{}", gumballMachine);

        gumballMachine.insertQuarter();
        gumballMachine.turnCrank();

        LOG.info("{}", gumballMachine);

        gumballMachine.insertQuarter();
        gumballMachine.turnCrank();


        gumballMachine.refill(5);
        gumballMachine.insertQuarter();
        gumballMachine.turnCrank();

        LOG.info("{}", gumballMachine);

        gumballMachine.insertQuarter();
        gumballMachine.ejectQuarter();
        LOG.info("{}", gumballMachine);
    }

}
