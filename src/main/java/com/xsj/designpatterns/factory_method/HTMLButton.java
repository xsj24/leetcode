package com.xsj.designpatterns.factory_method;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HTMLButton implements Button {

    private final static Logger LOG = LoggerFactory.getLogger(HTMLButton.class);

    @Override
    public void render() {
        LOG.info("HTMLButton#render");
    }
}
