package com.xsj.designpatterns.factory_method;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class Dialog {

    private final Logger LOG = LoggerFactory.getLogger(this.getClass());

    abstract Button createButton();

    public void render() {
        LOG.info("{}#render", this.getClass().getSimpleName());
        // Call the factory method to create a product object
        Button button = createButton();
        button.render();
    }

}
