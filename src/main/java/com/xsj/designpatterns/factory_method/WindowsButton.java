package com.xsj.designpatterns.factory_method;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WindowsButton implements Button {

    private final static Logger LOG = LoggerFactory.getLogger(WindowsButton.class);

    @Override
    public void render() {
        LOG.info("WindowsButton#render");
    }
}