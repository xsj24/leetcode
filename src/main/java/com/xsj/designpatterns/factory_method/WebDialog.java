package com.xsj.designpatterns.factory_method;


public class WebDialog extends Dialog {


    @Override
    Button createButton() {
        return new HTMLButton();
    }
}
