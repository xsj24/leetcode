package com.xsj.designpatterns.factory_method;

public interface Button {
    void render();
}
