package com.xsj.designpatterns.factory_method;

public class Application {

    private final Dialog dialog;

    public Application(String os) {
        if ("Windows".equals(os)) {
            this.dialog = new WindowsDialog();
        } else if ("Web".equals(os)) {
            this.dialog = new WebDialog();
        } else {
            throw new IllegalArgumentException("illegal os: " + os);
        }
    }

    public void start() {
        dialog.render();
    }

    public static void main(String[] args) {
        Application app = new Application("Windows");
        app.start();
    }

}
