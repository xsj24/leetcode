package com.xsj.designpatterns.factory_method;


public class WindowsDialog extends Dialog {

    @Override
    Button createButton() {
        return new WindowsButton();
    }

}
