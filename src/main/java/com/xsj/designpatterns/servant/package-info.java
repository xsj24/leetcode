/**
 *
 * Servant is used for providing some behavior to a group of classes. Instead of defining that behavior in each class
 * - or when we cannot factor out this behavior in the common parent class - it is defined once in the Servant.
 *
 * Created by dengx.
 * Date: 2017/10/17
 * Time: 1:34
 */
package com.xsj.designpatterns.servant;