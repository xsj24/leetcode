package com.xsj.designpatterns.memento;

import java.util.Scanner;

public class ApplicationMain {

    public static void main(String[] args) {
        InputText inputText = new InputText();
        SnapshotHolder snapshotsHolder = new SnapshotHolder();
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()) {
            String input = scanner.next();
            switch (input) {
                case ":list":
                    System.out.println(inputText.getText());
                    break;
                case ":undo":
                    Snapshot snapshot = snapshotsHolder.popSnapshot();
                    inputText.restoreSnapshot(snapshot);
                    break;
                default:
                    snapshotsHolder.pushSnapshot(inputText.createSnapshot());
                    inputText.append(input);
                    break;
            }
        }
    }
}