package com.xsj.nowcoder.coding_interviews;

import java.util.ArrayList;

/**
 * 输入一个链表，从尾到头打印链表节点值
 */
public class PrintListFromTailToHead {

    private PrintListFromTailToHead() {}

    public static class ListNode {
        int val;
        ListNode next;

        ListNode(int val) {
            this.val = val;
        }
    }

    public static ArrayList<Integer> printListFromTailToHead(ListNode listNode) {
        ArrayList<Integer> res = new ArrayList<>();
        add(res, listNode);
        return res;
    }

    private static void add(ArrayList<Integer> arr, ListNode listNode) {
        if (listNode != null) {
            add(arr, listNode.next);
            arr.add(listNode.val);
        }
    }

}
