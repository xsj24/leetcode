package com.xsj.interview.sort;

import java.util.Comparator;

public interface Sortlet<T> {

    void sort(T[] a, Comparator<? super T> c);

}
