package com.xsj.interview.sort;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.Comparator;

public class InsertSort<T> implements Sortlet<T> {

    static final Logger LOG = LoggerFactory.getLogger(InsertSort.class);

    @Override
    public void sort(T[] a, Comparator<? super T> c) {
        int len = a.length;
        if (len == 0) {
            return;
        }
        int count = 0;
        for (int i = 1; i < len; ++ i) {
            T value = a[i];
            int j = i - 1;
            for (; j >= 0; --j) {
                if (c.compare(a[j], value) > 0) {
                    a[j + 1] = a[j];  // 数据移动
                    ++count;
                } else {
                    break;
                }
            }
            a[j+1] = value;  // 插入数据
        }
        LOG.info("move count: {}", count);
    }
}
