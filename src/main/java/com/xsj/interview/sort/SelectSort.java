package com.xsj.interview.sort;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Comparator;


public class SelectSort<T> implements Sortlet<T> {

    static final Logger LOG = LoggerFactory.getLogger(SelectSort.class);

    @Override
    public void sort(T[] a, Comparator<? super T> c) {
        int len = a.length;
        if (len == 0) {
            return;
        }
        int idx;
        int count = 0;
        for (int i = 0; i < len - 1; ++i) {
            idx = i;
            for (int j = i + 1; j < len; ++j) {
                if (c.compare(a[idx], a[j]) > 0) {
                    idx = j;
                }
            }
            T tmp = a[idx];
            a[idx] = a[i];
            a[i] = tmp;
            ++count;
        }
        LOG.info("swap count: {}", count);
    }
}
