package com.xsj.interview.sort;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Array;
import java.util.Comparator;

public class RecursiveMergeSort<T> implements Sortlet<T> {

    static final Logger LOG = LoggerFactory.getLogger(RecursiveMergeSort.class);

    @Override
    public void sort(T[] a, Comparator<? super T> c) {
        @SuppressWarnings("unchecked")
        T[] result = (T[]) Array.newInstance(a.getClass().getComponentType(), a.length);
        mergeSort(a, c, 0, a.length - 1, result);

    }


    private void mergeSort(T[] a, Comparator<? super T> c, int start, int end, T[] result) {
        // 递归终止条件
        if (start >= end) {
            return;
        }
        int mid = (start + end) / 2;
        mergeSort(a, c, start, mid, result);
        mergeSort(a, c, mid + 1, end, result);
        merge(a, c, start, mid, mid + 1, end, result);
    }

    private void merge(T[] a, Comparator<? super T> c, int s1, int e1, int s2, int e2, T[] result) {
        int k = s1;
        int start = s1;
        while (s1 <= e1 && s2 <= e2) {
            result[k++] = c.compare(a[s1], a[s2]) <= 0 ? a[s1++] : a[s2++];
        }
        // 判断哪个子数组中还剩余数据
        while (s1 <= e1) {
            result[k++] = a[s1++];
        }
        while (s2 <= e2) {
            result[k++] = a[s2++];
        }
        // 将临时数组拷贝回原始数组
        System.arraycopy(result, start, a, start, e2 + 1 - start);
    }

}
