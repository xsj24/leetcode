package com.xsj.interview.sort.external;

import java.io.BufferedReader;
import java.io.IOException;

public final class BinaryFileBuffer implements IOStringStack {

    private BufferedReader bufferedReader;

    private String cache;

    public BinaryFileBuffer(BufferedReader r) throws IOException {
        this.bufferedReader = r;
        reload();
    }

    @Override
    public void close() throws IOException {
        this.bufferedReader.close();
    }

    @Override
    public boolean empty() {
        return this.cache == null;
    }

    @Override
    public String peek() {
        return this.cache;
    }

    @Override
    public String pop() throws IOException {
        String answer = peek();// make a copy
        reload();
        return answer;
    }

    private void reload() throws IOException {
        this.cache = this.bufferedReader.readLine();
    }

}