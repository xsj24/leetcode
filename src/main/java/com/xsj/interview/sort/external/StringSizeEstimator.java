package com.xsj.interview.sort.external;

public abstract class StringSizeEstimator {

    private StringSizeEstimator() {}


    private static boolean IS_64_BIT_JVM = true;

    private static int OBJ_OVERHEAD;


    static {
        String arch = System.getProperty("sun.arch.data.model");
        if (null != arch) {
            if (arch.contains("32")) {
                IS_64_BIT_JVM = false;
            }
        }

        // the calculation below is not accurate
        int OBJ_HEADER = IS_64_BIT_JVM ? 16 : 8;
        int ARR_HEADER = IS_64_BIT_JVM ? 24 : 12;
        int OBJ_REF = IS_64_BIT_JVM ? 8 : 4;
        int INT_FIELDS = 12;
        OBJ_OVERHEAD =  OBJ_HEADER + OBJ_REF + ARR_HEADER + INT_FIELDS;

    }

    /**
     * Estimates the size of a {@link String} object in bytes.
     */
    public static long estimateSizeOf(String s) {
        return  s.length() * 2 + OBJ_OVERHEAD;
    }


}
