package com.xsj.interview.sort.external;

import java.io.IOException;

public interface IOStringStack {

    void close() throws IOException;

    boolean empty();

    String peek();

    String pop() throws IOException;

}