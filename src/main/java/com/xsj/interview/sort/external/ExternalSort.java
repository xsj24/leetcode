package com.xsj.interview.sort.external;

import java.io.*;
import java.nio.charset.Charset;
import java.util.*;
import java.util.stream.Collectors;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class ExternalSort {

    public static final int DEFAULT_MAX_TEMP_FILES = 1024;

    private static void displayUsage() {
        System.out.println("ExternalSort inputfile outputfile");
        System.out.println("Flags are:");
        System.out.println("-v or --verbose: verbose output");
        System.out.println("-d or --distinct: prune duplicate lines");
        System.out.println("-t or --maxtmpfiles (followed by an integer): specify an upper bound on the number of temporary files");
        System.out.println("-c or --charset (followed by a charset code): specify the character set to use (for sorting)");
        System.out.println("-z or --gzip: use compression for the temporary files");
        System.out.println("-H or --header (followed by an integer): ignore the first few lines");
        System.out.println("-s or --store (following by a path): where to store the temporary files");
        System.out.println("-h or --help: display this message");
    }


    public static void main(final String[] args) throws IOException {
        Conf conf = new Conf();
        for (int param = 0; param < args.length; ++param) {
            if (args[param].equals("-v") || args[param].equals("--verbose")) {
                conf.verbose = true;
            } else if ((args[param].equals("-h") || args[param].equals("--help"))) {
                displayUsage();
                return;
            } else if ((args[param].equals("-d") || args[param].equals("--distinct"))) {
                conf.distinct = true;
            } else if ((args[param].equals("-t") || args[param].equals("--maxtmpfiles"))
                    && args.length > param + 1) {
                param++;
                conf.maxTempFiles = Integer.parseInt(args[param]);
                if (conf.maxTempFiles < 0) {
                    err("maxtmpfiles should be positive");
                    return;
                }
            } else if ((args[param].equals("-c") || args[param]
                    .equals("--charset"))
                    && args.length > param + 1) {
                param++;
                conf.charset = Charset.forName(args[param]);
            } else if ((args[param].equals("-z") || args[param]
                    .equals("--gzip"))) {
                conf.usegzip = true;
            } else if ((args[param].equals("-H") || args[param]
                    .equals("--header")) && args.length > param + 1) {
                param++;
                conf.headerLineNum = Integer.parseInt(args[param]);
                if (conf.headerLineNum < 0) {
                    err("headersize should be positive");
                    return;
                }
            } else if ((args[param].equals("-s") || args[param].equals("--store")) && args.length > param + 1) {
                param++;
                conf.tempFileStore = new File(args[param]);
            } else {
                if (conf.inputFilename == null) {
                    conf.inputFilename = args[param];
                } else if (conf.outputFilename == null) {
                    conf.outputFilename = args[param];
                } else {
                    err("Unparsed: " + args[param]);
                    return;
                }
            }
        }
        if (conf.outputFilename == null) {
            err("please provide input and output file names");
            displayUsage();
            return;
        }


        List<File> splitFiles = sortInBatch(conf);
        if (conf.isVerbose()) {
            System.out.println("created " + splitFiles.size() + " tmp files");
        }

        mergeSortedFiles(splitFiles, conf);

    }

    private static void mergeSortedFiles(List<File> files, Conf conf) throws IOException {
        ArrayList<IOStringStack> buffers = new ArrayList<>();
        for (File f : files) {
            InputStream in = new FileInputStream(f);
            BufferedReader br;
            if (conf.usegzip) {
                br = new BufferedReader(
                        new InputStreamReader(
                                new GZIPInputStream(in,
                                        2048), conf.getCharset()));
            } else {
                br = new BufferedReader(new InputStreamReader(
                        in, conf.getCharset()));
            }
            BinaryFileBuffer bfb = new BinaryFileBuffer(br);
            buffers.add(bfb);
        }

        PriorityQueue<IOStringStack> pq = new PriorityQueue<>(20, (l, r) -> conf.getComparator().compare(l.peek(), r.peek()));
        for (IOStringStack buffer : buffers) {
            if (!buffer.empty()) {
                pq.add(buffer);
            }
        }
        long rowCounter = 0;
        try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(conf.getOutputFilename()), conf.getCharset()))) {
            if (!conf.isDistinct()) {
                while (pq.size() > 0) {
                    IOStringStack buf = pq.poll();
                    String row = buf.pop();
                    writer.write(row);
                    writer.newLine();
                    ++rowCounter;
                    if (!buf.empty()) {
                        pq.add(buf);
                    }
                }
            } else {
                String lastLine = null;
                if (pq.size() > 0) {
                    IOStringStack buf = pq.poll();
                    lastLine = buf.pop();
                    writer.write(lastLine);
                    writer.newLine();
                    ++rowCounter;
                    if (!buf.empty()) {
                        pq.add(buf);
                    }
                }
                while (pq.size() > 0) {
                    IOStringStack bfb = pq.poll();
                    String row = bfb.pop();
                    // Skip duplicate lines
                    if  (conf.getComparator().compare(row, lastLine) != 0) {
                        writer.write(row);
                        writer.newLine();
                        lastLine = row;
                    }
                    ++rowCounter;
                    if (!bfb.empty()) {
                        pq.add(bfb); // add it back
                    }
                }
            }
            if (conf.isVerbose()) {
                System.out.println("结果文件行数: " + rowCounter);
            }
        } finally {
            for (IOStringStack buffer : buffers) {
                try {
                    buffer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        for (File f : files) {
            f.deleteOnExit();
        }

    }

    /**
     *
     * @return a list of temporary flat files
     * @throws IOException generic IO exception
     */
    private static List<File> sortInBatch(Conf conf) throws IOException {
        File inputFile = new File(conf.getInputFilename());

        long maxMemory = estimateAvailableMemory();
        long blockSize = estimateBestSizeOfBlocks(inputFile.length(), conf.getMaxTempFiles(), maxMemory);

        List<File> files = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(inputFile), conf.getCharset()))) {
            List<String> tmpList = new ArrayList<>();

            int counter = 0;
            String line = "";
            while (line != null) {
                long currentBlockSize = 0; // in bytes
                while ((currentBlockSize < blockSize) && ((line = reader.readLine()) != null)) {
                    // as long as you have enough memory
                    if (counter < conf.getHeaderLineNum()) {
                        ++counter;
                        continue;
                    }
                    tmpList.add(line);
                    currentBlockSize += StringSizeEstimator.estimateSizeOf(line);
                }
                files.add(sortAndSave(tmpList, conf));
                tmpList.clear();
            }

        }

        return files;
    }

    private static File sortAndSave(List<String> tmpList, Conf conf) throws IOException {
        if (conf.isParallel()) {
            tmpList = tmpList.parallelStream()
                    .sorted(conf.getComparator())
                    .collect(Collectors.toCollection(ArrayList::new));
        } else {
            tmpList.sort(conf.getComparator());
        }
        File newTmpFile = File.createTempFile("sortInBatch",
                ".flat", conf.getTempFileStore());
        OutputStream out = new FileOutputStream(newTmpFile);
        if (conf.isUsegzip()) {
            out = new GZIPOutputStream(out, 2048);
        }
        try (BufferedWriter  writer = new BufferedWriter(new OutputStreamWriter(out, conf.getCharset()))) {
            if (!conf.isDistinct()) {
                for (String str : tmpList) {
                    writer.write(str);
                    writer.newLine();
                }
            } else {
                String lastLine = null;
                Iterator<String> iterator = tmpList.iterator();
                if (iterator.hasNext()) {
                    lastLine = iterator.next();
                    writer.write(lastLine);
                    writer.newLine();
                }
                while (iterator.hasNext()) {
                    String cur = iterator.next();
                    if (conf.getComparator().compare(cur, lastLine) != 0) {
                        writer.write(cur);
                        writer.newLine();
                        lastLine = cur;
                    }
                }
            }
        }
        return newTmpFile;
    }

    /**
     * we divide the file into small blocks. If the blocks are too small, we shall create too many temporary files.
     * If they are too big, we shall be using too much memory
     */
    private static long estimateBestSizeOfBlocks(long sizeOfFile, int maxTmpFiles, long maxMemory) {
        long blockSize = sizeOfFile / maxTmpFiles
                + (sizeOfFile % maxTmpFiles == 0 ? 0 : 1);

        // 不要创建太多文件
        if (blockSize < maxMemory / 2) {
            blockSize = maxMemory / 2;
        }
        return blockSize;
    }


    private static long estimateAvailableMemory() {
        Runtime r = Runtime.getRuntime();
        long allocatedMemory = r.totalMemory() - r.freeMemory();
        return r.maxMemory() - allocatedMemory;
    }

    private static void err(String msg) {
        System.err.println(msg);
    }

    static class Conf {
        private boolean verbose = false;
        private boolean distinct = false;
        private int maxTempFiles = DEFAULT_MAX_TEMP_FILES;
        private Charset charset = Charset.defaultCharset();
        private String inputFilename;
        private String outputFilename;
        private File tempFileStore;
        private boolean usegzip = false;
        private boolean parallel = true;
        private int headerLineNum = 0;
        private Comparator<String> comparator = DEFAULT_COMPARATOR;

        public boolean isVerbose() {
            return verbose;
        }

        public void setVerbose(boolean verbose) {
            this.verbose = verbose;
        }

        public boolean isDistinct() {
            return distinct;
        }

        public void setDistinct(boolean distinct) {
            this.distinct = distinct;
        }

        public int getMaxTempFiles() {
            return maxTempFiles;
        }

        public void setMaxTempFiles(int maxTempFiles) {
            this.maxTempFiles = maxTempFiles;
        }

        public Charset getCharset() {
            return charset;
        }

        public void setCharset(Charset charset) {
            this.charset = charset;
        }

        public String getInputFilename() {
            return inputFilename;
        }

        public void setInputFilename(String inputFilename) {
            this.inputFilename = inputFilename;
        }

        public String getOutputFilename() {
            return outputFilename;
        }

        public void setOutputFilename(String outputFilename) {
            this.outputFilename = outputFilename;
        }

        public File getTempFileStore() {
            return tempFileStore;
        }

        public void setTempFileStore(File tempFileStore) {
            this.tempFileStore = tempFileStore;
        }

        public boolean isUsegzip() {
            return usegzip;
        }

        public void setUsegzip(boolean usegzip) {
            this.usegzip = usegzip;
        }

        public boolean isParallel() {
            return parallel;
        }

        public void setParallel(boolean parallel) {
            this.parallel = parallel;
        }

        public int getHeaderLineNum() {
            return headerLineNum;
        }

        public void setHeaderLineNum(int headerLineNum) {
            this.headerLineNum = headerLineNum;
        }

        public Comparator<String> getComparator() {
            return comparator;
        }

        public void setComparator(Comparator<String> comparator) {
            this.comparator = comparator;
        }
    }


    /**
     * default comparator between strings.
     */
    static final Comparator<String> DEFAULT_COMPARATOR = String::compareTo;

}
