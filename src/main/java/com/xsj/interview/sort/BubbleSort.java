package com.xsj.interview.sort;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Comparator;

public class BubbleSort<T> implements Sortlet<T> {

    static final Logger LOG = LoggerFactory.getLogger(BubbleSort.class);

    @Override
    public void sort(T[] a, Comparator<? super T> c) {
        int len = a.length;
        if (len == 0) {
            return;
        }
        int count = 0;
        T tmp;
        for (int i = 1; i < len; ++i) {
            boolean swap = false;
            for (int j = 0; j < len - i; ++j) {
                if (c.compare(a[j], a[j + 1]) > 0) {
                    tmp = a[j];
                    a[j] = a[j + 1];
                    a[j + 1] = tmp;
                    swap = true;
                    ++ count;
                }
            }
            if (!swap) {
                break;
            }
        }
        LOG.info("swap count: {}", count);
    }



}
