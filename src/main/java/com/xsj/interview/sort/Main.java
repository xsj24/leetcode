package com.xsj.interview.sort;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class Main {

    static final Logger LOG = LoggerFactory.getLogger(Main.class);


    public static void main(String[] args) {
//        sortNumberrs();

        sortObjects();

    }

    private static void sortObjects() {
        ArrayList<Sortlet<Person>> sortObjs = new ArrayList<>();
        sortObjs.add(new BubbleSort<>());
        sortObjs.add(new InsertSort<>());
        sortObjs.add(new SelectSort<>());
        sortObjs.add(new RecursiveMergeSort<>());

        for (Sortlet<Person> sortObj : sortObjs) {
            Person[] persons = {
                    new Person("a1", 5),
                    new Person("a2", 8),
                    new Person("a3", 5),
                    new Person("a4", 2),
                    new Person("a5", 9),
                    new Person("a6", 1),
            };

            final int[] comCnds = {0};
            sortObj.sort(persons, Comparator.comparing((Person person) -> {
                comCnds[0]++;
                return person.getAge();
            }));
            LOG.info("compare count: {}", comCnds[0]);
            LOG.info("persons: {}\n", Arrays.toString(persons));
        }
    }

    private static void sortNumberrs() {
        ArrayList<Sortlet<Integer>> sortlets = new ArrayList<>();
        sortlets.add(new BubbleSort<>());
        sortlets.add(new InsertSort<>());
        sortlets.add(new SelectSort<>());

        for (Sortlet<Integer> sortlet : sortlets) {
            Integer[] arr = {1, 9, 3, 5, 6, 4, 7, -8, 45, 12, 12, 77, 33, 22, 10, 78, -9};
            final int[] comCnds = {0};
            sortlet.sort(arr, (o1, o2) -> {
                comCnds[0]++;
                return o2.compareTo(o1);
            });
            LOG.info("compare count: {}", comCnds[0]);
            LOG.info("arr: {}\n", Arrays.toString(arr));
        }
    }


    static class Person {

        private String name;
        private int age;

        public Person(String name, int age) {
            this.name = name;
            this.age = age;
        }

        public String getName() {
            return name;
        }

        public int getAge() {
            return age;
        }

        @Override
        public String toString() {
            return "{" +
                    "name='" + name + '\'' +
                    ", age=" + age +
                    '}';
        }
    }

}
