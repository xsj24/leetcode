package com.xsj.interview.array;

public class RotateMatrix {


    public static void rotate(int[][] matrix) {
        int len = matrix.length;
        if (len == 0 || len != matrix[0].length) {
            return;
        }
        // 转置
        for (int i = 0; i < len; ++i) {
            for (int j = i + 1; j < len; j++) {
                int tmp = matrix[i][j];
                matrix[i][j] = matrix[j][i];
                matrix[j][i] = tmp;
            }
        }
        // 交换列
        for (int i = 0; i < len; i++) {
            for (int j = 0; j < len / 2; j++) {
                int tmp = matrix[i][j];
                matrix[i][j] = matrix[i][len - 1 - j];
                matrix[i][len - 1 - j] = tmp;
            }
        }

    }


}
