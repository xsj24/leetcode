package com.xsj.interview.array;

public class Commons {

    public static void swap(int[] arr, int i, int j) {
        if (i == j) {
            return;
        }
        int tmp = arr[i];
        arr[i] = arr[j];
        arr[j] = tmp;
    }


    //  put all positive numbers together in one side
    public static int partition(int[] arr) {
        int len = arr.length;
        int q = -1;
        for (int i = 0; i < len; i++) {
            if (arr[i] <= 0) {
                continue;
            }
            swap(arr, ++q, i);
        }
        return q;
    }

}
