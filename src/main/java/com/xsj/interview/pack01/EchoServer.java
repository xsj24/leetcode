package com.xsj.interview.pack01;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * 用Java的套接字编程实现一个多线程的回显（echo）服务器。
 */
public class EchoServer {

    static final int PORT = 5566;

    public static void main(String[] args) {
        try {
            ServerSocket server = new ServerSocket(PORT);
            while (true) {
                Socket clientSocket = server.accept();
                EchoHandler echoHandler = new EchoHandler(clientSocket);
                echoHandler.start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static class EchoHandler extends Thread {

        Socket client;

        EchoHandler (Socket client) {
            this.client = client;
        }

        public void run () {
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(client.getInputStream()));
                 PrintWriter writer = new PrintWriter(client.getOutputStream(), true)) {

                writer.println("[type 'bye' to disconnect]");
                while (true) {
                    String line = reader.readLine();
                    if (line.trim().equals("bye")) {
                        writer.println("bye!");
                        break;
                    }
                    writer.println("[echo] " + line);
                }
            }
            catch (Exception e) {
                System.err.println("Exception caught: client disconnected.");
            }
            finally {
                try { client.close(); }
                catch (Exception e ){ ; }
            }
        }
    }

}
