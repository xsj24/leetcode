package com.xsj.interview.synchronization;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * lock 和 condition 实现同步容器
 */
public class SynchronizedContainerII<T> {

    private final Queue<T> queue = new LinkedList<>();

    private final static int MAX = 10;

    private int count = 0;

    private Lock lock = new ReentrantLock();

    private Condition notEmpty = lock.newCondition();

    private Condition notFull = lock.newCondition();

    public void put(T t) {
        try {
            lock.lock();
            while (count == MAX) {
                notFull.await();
            }
            ++count;
            queue.offer(t);
            notEmpty.signalAll();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }

    public T get() {
        T res = null;
        try {
            lock.lock();
            while (count == 0) {
                notEmpty.await();
            }
            --count;
            res = queue.poll();
            notFull.signalAll();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
        return res;
    }

    public static void main(String[] args) {
        final SynchronizedContainerII<Integer> container = new SynchronizedContainerII<>();

        for (int i = 0; i < 10; i++) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    for (int j = 0; j < 5; j++) {
                        System.out.println(Thread.currentThread().getName() + " " + container.get());
                    }
                }
            }, "c" + i).start();
        }

        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < 2; i++) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    for (int j = 0; j < 25; j++) {
                        container.put(j);
                    }
                }
            }, "p" + i).start();
        }
    }

}
