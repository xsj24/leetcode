package com.xsj.interview.synchronization;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.TimeUnit;

/**
 * 同步容器
 */
public class SynchronizedContainer<T> {

    private final Queue<T> list = new LinkedList<>();

    private final static int MAX = 10;

    private int count = 0;

    public synchronized void put(T t) {
        while (count == MAX) {
            try {
                this.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        list.offer(t);
        ++count;
        this.notifyAll();
    }

    public synchronized T get() {
        while (count == 0) {
            try {
                this.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        T poll = list.poll();
        --count;
        this.notifyAll();
        return poll;
    }

    public synchronized int getCount() {
        return count;
    }

    public static void main(String[] args) {
        final SynchronizedContainer<Integer> container = new SynchronizedContainer<>();

        for (int i = 0; i < 10; i++) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    for (int j = 0; j < 5; j++) {
                        System.out.println(Thread.currentThread().getName() + " " + container.get());
                    }
                }
            }, "c" + i).start();
        }

        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < 2; i++) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    for (int j = 0; j < 25; j++) {
                        container.put(j);
                    }
                }
            }, "p" + i).start();
        }


    }

}
