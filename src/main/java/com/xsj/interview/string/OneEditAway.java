package com.xsj.interview.string;

/**
 * pale, ple -> true
 * pales, pale -> true
 * pale, bale -> true
 * pale, bae -> false
 */
public class OneEditAway {

    public static boolean solute01(String first, String second) {
        if (first.length() == second.length()) {
            return oneEditReplace(first, second);
        } else if (first.length() + 1 == second.length()) {
            return oneEditInsert(first, second);
        } else if (first.length() - 1 == second.length()) {
            return oneEditInsert(second, first);
        }
        return false;
    }

    /**
     * Check if you can insert a character into s1 to make s2.
     */
    private static boolean oneEditInsert(String first, String second) {
        int idx01 = 0;
        int idx02 = 0;
        while (idx02 < second.length() && idx01 < first.length()) {
            if (second.charAt(idx02) != first.charAt(idx01)) {
                if (idx01 != idx02) {
                    return false;
                }
                ++idx02; // 忽略第一个不同点
            } else {
                ++idx01;
                ++idx02;
            }
        }

        return true;
    }

    private static boolean oneEditReplace(String first, String second) {
        boolean foundDiff = false;
        for (int i = 0; i < first.length(); i++) {
            if (first.charAt(i) != second.charAt(i)) {
                if (foundDiff) {
                    return false;
                }
                foundDiff = true;
            }
        }
        return true;
    }


}
