package com.xsj.interview.string;

public class StringCompression {


    public static String compress(String str) {
        int length = countCompression(str);
        if (length >= str.length()) {
            return str;
        }
        StringBuilder builder = new StringBuilder(length);
        int court = 0;
        for (int i = 0; i < str.length(); ++i) {
            ++court;
            if (i + 1 == str.length() || str.charAt(i) != str.charAt(i + 1)) {
                builder.append(str.charAt(i))
                        .append(court);
                court = 0;
            }
        }
        return builder.length() < str.length() ? builder.toString() : str;
    }


    private static int countCompression(String str) {
        int compressedLength = 0;
        int countConsecutive = 0;
        for (int i = 0; i < str.length(); ++i) {
            ++countConsecutive;
            if (i + 1 == str.length() || str.charAt(i) != str.charAt(i + 1)) {
                compressedLength += 1 + String.valueOf(countConsecutive).length();
                countConsecutive = 0;
            }
        }
        return compressedLength;
    }

}
