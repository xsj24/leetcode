package com.xsj.interview.string;

import com.xsj.leetcode.util.ListNode;

// 字符串是否为回文串
public abstract class PalindromeCheck {

    private PalindromeCheck() {}


    public static boolean isPalindrome(String str) {
        int i = 0;
        int j = str.length() - 1;
        while (i < j) {
            if (str.charAt(i++) != str.charAt(j--)) {
                return false;
            }
        }
        return true;
    }

    public static boolean isPalindromeRecursively(String str) {
        return recursiveCall(str, 0, str.length() - 1);
    }

    // 递归解决
    private static boolean recursiveCall(String str, int i, int j) {
        if (i == j) {
            return true;
        }
        if (str.charAt(i) != str.charAt(j)) {
            return false;
        }
        if (++i >= --j) {
            return true;
        }
        return recursiveCall(str, i, j);
    }

    // 左侧指针
    static ListNode left;

    // 是否为回文链表
    public static boolean isPalindrome(ListNode<Integer> head) {
        left = head;
        return traverse(head);
    }

    // 时间空间复杂度都为 O(n)
    private static boolean traverse(ListNode<Integer> right) {
        if (right == null) {
            return true;
        }
        boolean res = traverse(right.getNext());
        // 后序遍历代码
        res = res && (right.getItem().equals(left.getItem()));
        left = left.getNext();
        return res;
    }


}
