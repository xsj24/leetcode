package com.xsj.math.congruence;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;


public class IntroTest {


    /**
     * a - b 能被 n 整除，则 a 、b 模 n 同余
     *
     * proof:
     *  设 a - b = kn => a = b + kn
     *  两边对 m 取余
     *      => a % m = b % m
     *
     */
    @Test
    public void testModByN() {
        {
            int a = 14;
            int b = 2;
            int n = 12;
            assertThat(divisibleBy(a, b, n), is(true));
            assertThat(a % n, is(b % n));
        }

        {
            int a = 14;
            int b = 3;
            int n = 12;
            assertThat(divisibleBy(a, b, n), is(false));
            assertThat(a % n, not(b % n));
        }

    }

    private boolean divisibleBy(int a, int b, int n) {
        return (a - b) % n == 0;
    }
}
