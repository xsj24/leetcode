package com.xsj.designpatterns.fsm;

import org.junit.Test;

import static org.junit.Assert.*;

public class SimpleIniParserTest {

    @Test
    public void testParse() {

        String str = "[GRP]\n" +
                "\tname = def  \n" +
                "data = 2016.11.29 \r\n" +
                "; this is a comment \r\n" +
                "str = this is a test \n\n" +
                "[zhangshan] \n\n" +
                "; 测试\n" +
                "name = pengjun \n\n" +
                "data = 3355456\n";
        SimpleIniParser.parse(str);
    }
}