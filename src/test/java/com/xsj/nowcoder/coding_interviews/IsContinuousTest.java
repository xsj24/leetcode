package com.xsj.nowcoder.coding_interviews;

import org.junit.Test;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

/**
 * Created by dengx.
 * Date: 2017/11/12
 * Time: 23:18
 */
public class IsContinuousTest {

    @Test
    public void testIsContinuous() throws Exception {
        assertThat(IsContinuous.isContinuous01(new int[]{1, 3, 0, 0, 5}), is(true));
        assertThat(IsContinuous.isContinuous01(new int[]{1, 3, 0, 0, 6}), is(false));
    }
}