package com.xsj.nowcoder.coding_interviews;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.xsj.nowcoder.coding_interviews.PrintListFromTailToHead.*;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class PrintListFromTailToHeadTest {

    ListNode head = null;

    @Before
    public void before() {
        int[] datas = {2, 3, 7, 5, 6};
        ListNode dummyHead = new ListNode(0);
        ListNode p = dummyHead;
        for (int data : datas) {
            p.next = new ListNode(data);
            p = p.next;
        }
        head = dummyHead.next;
    }

    @Test
    public void testPrintListFromTailToHead() throws Exception {
        List<Integer> integers = printListFromTailToHead(head);
        assertThat(integers, equalTo(Arrays.asList(6, 5, 7, 3, 2)));
    }
}