package com.xsj.leetcode.medium;

import org.junit.Test;

import java.lang.ref.SoftReference;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

/**
 * Created by dengx.
 * Date: 2017/7/5
 * Time: 22:32
 */
public class SortColorsTest {

    @Test
    public void testSortColors() throws Exception {
        int[] arr = {2, 1, 0};
        SortColors.sortColors(arr);
        assertThat(arr, equalTo(new int[]{0, 1, 2}));
    }
}