package com.xsj.leetcode.medium;

import com.xsj.leetcode.util.TreeNode;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class SumRootToLeafNumbersTest {

    @Test
    public void testSumNumbers() {

        assertThat(SumRootToLeafNumbers.sumNumbers(null), is(0));
        assertThat(SumRootToLeafNumbers.sumNumbers(new TreeNode(9)), is(9));

        TreeNode root = new TreeNode(1);
        root.setLeft(new TreeNode(2));
        root.setRight(new TreeNode(3));

        assertThat(SumRootToLeafNumbers.sumNumbers(root), is(25));

    }

}