package com.xsj.leetcode.medium;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import static com.xsj.leetcode.medium.DesignLogStorageSystem.*;

/**
 * Created by dengx.
 * Date: 2017/7/4
 * Time: 22:58
 */
public class DesignLogStorageSystemTest {


    @Test
    public void test() {
        LogSystem logSystem = new LogSystem();
        logSystem.put(1, "2017:01:01:23:59:59");
        logSystem.put(2, "2017:01:01:22:59:59");
        logSystem.put(3, "2016:01:01:00:00:00");

        List<Integer> year = logSystem.retrieve("2016:01:01:01:01:01", "2017:01:01:23:00:00", "Year");
        assertThat(year, equalTo(Arrays.asList(1, 2, 3)));

        List<Integer> hour = logSystem.retrieve("2016:01:01:01:01:01", "2017:01:01:23:00:00", "Hour");
        assertThat(hour, equalTo(Arrays.asList(1, 2)));
    }


}