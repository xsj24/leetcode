package com.xsj.leetcode.medium;

import com.xsj.leetcode.easy.ReverseLinkedList;
import org.junit.Before;
import org.junit.Test;

import static com.xsj.leetcode.medium.SwapNodesInPairs.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class SwapNodesInPairsTest {

    ListNode head = null;

    @Before
    public void before() {
        int[] datas = {2, 3, 4, 5, 6};
        ListNode dummyHead = new ListNode(0);
        ListNode p = dummyHead;
        for (int data : datas) {
            p.next = new ListNode(data);
            p = p.next;
        }
        head = dummyHead.next;
    }

    @Test
    public void testSwapPairs() throws Exception {
        ListNode listNode = SwapNodesInPairs.swapPairs(head);
        assertThat(listNode.val, equalTo(3));
    }
}