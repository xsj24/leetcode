package com.xsj.leetcode.medium;

import com.xsj.leetcode.medium.MergeIntervals.Interval;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class MergeIntervalsTest {

    @Test
    public void testMerge() throws Exception {
        // [1, 3], [2, 6], [8, 10], [15, 18]
        ArrayList<Interval> intervals = new ArrayList<>(Arrays.asList(new Interval(1, 3), new Interval(2, 6),
                new Interval(8, 10), new Interval(15, 18)));

        List<Interval> merge = MergeIntervals.merge01(intervals);
        assertThat(merge.get(0), equalTo(new Interval(1, 6)));
        assertThat(merge.get(1), equalTo(new Interval(8, 10)));
        assertThat(merge.get(2), equalTo(new Interval(15, 18)));
    }
}