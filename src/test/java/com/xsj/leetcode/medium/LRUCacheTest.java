package com.xsj.leetcode.medium;

import org.junit.Test;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class LRUCacheTest {


    @Test
    public void testLru() throws Exception {
        LRUCache.Cacheable cache = new LRUCache.LRUCache02(2);
        cache.put(1, 1);
        cache.put(2, 2);
        cache.put(3, 3);
        assertThat(cache.get(1), is(-1));
        assertThat(cache.size(), is(2));
    }


}