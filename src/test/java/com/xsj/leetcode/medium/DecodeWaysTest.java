package com.xsj.leetcode.medium;

import org.junit.Test;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class DecodeWaysTest {

    @Test
    public void testNumDecodings() throws Exception {
        assertThat(DecodeWays.numDecodings("1"), is(1));
        assertThat(DecodeWays.numDecodings("0"), is(0));
        assertThat(DecodeWays.numDecodings("11"), is(2));
        assertThat(DecodeWays.numDecodings("01"), is(0));
    }

}