package com.xsj.leetcode.medium;

import org.junit.Test;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class UniquePathsIITest {

    @Test
    public void testUniquePathsWithObstacles() throws Exception {
        assertThat(UniquePathsII.uniquePathsWithObstacles(new int[][]{{1}}), equalTo(0));
        int[][] ints = {
            {0, 0, 0},
            {0, 1, 0},
            {0, 0, 0},
        };
        assertThat(UniquePathsII.uniquePathsWithObstacles(ints), equalTo(2));
    }
}