package com.xsj.leetcode.medium;

import com.xsj.leetcode.util.ListHelper;
import com.xsj.leetcode.util.ListNode;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class PartitionListTest {

    ListNode<Integer> head = null;

    @Before
    public void before() {
        head = ListHelper.create(1, 4, 3, 2, 5, 2);
    }

    @Test
    public void partition() throws Exception {
        PartitionList.partition(head, 3);
        ListNode<Integer> expected = ListHelper.create(1, 2, 2, 4, 3, 5);
        assertThat(ListHelper.equalTo(head, expected), is(true));
    }

}