package com.xsj.leetcode.medium;

import org.junit.Test;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class SimplifyPathTest {

    @Test
    public void testSimplifyPath() throws Exception {
        assertThat(SimplifyPath.simplifyPath("/../"), equalTo("/"));
        assertThat(SimplifyPath.simplifyPath("/a/"), equalTo("/a"));
    }
}