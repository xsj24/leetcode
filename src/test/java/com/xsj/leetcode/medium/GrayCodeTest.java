package com.xsj.leetcode.medium;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;


public class GrayCodeTest {

    @Test
    public void grayCode() throws Exception {
        assertThat(GrayCode.grayCode(1), is(Arrays.asList(0, 1)));
        assertThat(GrayCode.grayCode(2), is(Arrays.asList(0, 1, 3, 2)));
        assertThat(GrayCode.grayCodeI(3), is(Arrays.asList(0, 1, 3, 2, 6, 7, 5, 4)));
    }

}