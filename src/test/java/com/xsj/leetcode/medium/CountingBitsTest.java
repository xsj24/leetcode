package com.xsj.leetcode.medium;

import org.junit.Test;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

/**
 * Created by dengx.
 * Date: 2017/11/12
 * Time: 21:54
 */
public class CountingBitsTest {

    @Test
    public void testCountBits01() throws Exception {
        assertThat(CountingBits.countBits01(5), equalTo(new int[]{0, 1, 1, 2, 1, 2}));
    }
}