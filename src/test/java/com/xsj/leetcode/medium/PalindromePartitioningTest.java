package com.xsj.leetcode.medium;

import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class PalindromePartitioningTest {

    @Test
    public void partition() {
        assertThat(PalindromePartitioning.partition("ab"), is(Collections.singletonList(Arrays.asList("a", "b"))));
        assertThat(PalindromePartitioning.partition("aab"), is(
                Arrays.asList(Arrays.asList("a", "a", "b"),
                Arrays.asList("aa", "b"))
        ));
    }


}