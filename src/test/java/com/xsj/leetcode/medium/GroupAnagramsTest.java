package com.xsj.leetcode.medium;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class GroupAnagramsTest {

    @Test
    public void groupAnagrams() throws Exception {
        String[] strs = {"eat", "tea", "tan", "ate", "nat", "bat"};
        List<List<String>> result = GroupAnagrams.groupAnagrams(strs);
        assertThat(result.size(), is(3));
    }

}