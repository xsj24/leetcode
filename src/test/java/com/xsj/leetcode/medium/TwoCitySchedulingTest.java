package com.xsj.leetcode.medium;

import org.junit.Test;

import static org.junit.Assert.*;

public class TwoCitySchedulingTest {

    @Test
    public void testTwoCitySchedCost() {
        int[][] costs = {{10, 20}, {30, 200}, {400, 50}, {30, 20}};
        System.out.println(TwoCityScheduling.twoCitySchedCost(costs));
    }
}