package com.xsj.leetcode.medium;

import org.junit.Test;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

/**
 * Created by dengx.
 * Date: 2018/1/25
 * Time: 22:27
 */
public class DivideTwoIntegersTest {

    @Test
    public void testDivide() throws Exception {
        assertThat(DivideTwoIntegers.divide(Integer.MIN_VALUE, -1), is(Integer.MAX_VALUE));
        assertThat(DivideTwoIntegers.divide(0, -1), is(0));
    }
}