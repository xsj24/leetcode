package com.xsj.leetcode.hard;

import org.junit.Test;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class LongestValidParenthesesTest {

    @Test
    public void testLongestValidParentheses() throws Exception {
        assertThat(LongestValidParentheses.longestValidParentheses(""), equalTo(0));
        assertThat(LongestValidParentheses.longestValidParentheses(")))()"), equalTo(2));
        assertThat(LongestValidParentheses.longestValidParentheses(")))()))()()"), equalTo(4));
        assertThat(LongestValidParentheses.longestValidParentheses("(()()"), equalTo(4));
    }
}