package com.xsj.leetcode.premium.hard;

import org.junit.Test;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

/**
 * Created by dengx.
 * Date: 2017/10/17
 * Time: 1:02
 */
public class SplitArrayLargestSumTest {

    @Test
    public void testSplitArray() throws Exception {
        int[] ints = {1, 2, 3, 4, 5};
        int res = new SplitArrayLargestSum().splitArray(ints, 4);
        assertThat(res, equalTo(5));
    }
}