package com.xsj.leetcode.easy;

import org.junit.Test;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class MaximumSubArrayTest {

    @Test
    public void maxSubArray() {
        assertThat(MaximumSubArray.maxSubArray(new int[]{-2, 1, -3, 4, -1, 2, 1, -5, 4}), is(6));
        assertThat(MaximumSubArray.maxSubArray(new int[]{4}), is(4));
    }
}