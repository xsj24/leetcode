package com.xsj.leetcode.easy;

import org.junit.Test;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;


public class MoveZeroesTest {

    @Test
    public void moveZeros() {
        {
            int[] ints = {1, 0, 0, 2};
            MoveZeroes.moveZeros(ints);
            assertThat(ints, is(new int[]{1, 2, 0, 0}));
        }
    }


}