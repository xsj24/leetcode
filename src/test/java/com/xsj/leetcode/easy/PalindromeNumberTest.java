package com.xsj.leetcode.easy;

import org.junit.Test;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class PalindromeNumberTest {

    @Test
    public void isPalindrome() throws Exception {
        assertThat(PalindromeNumber.isPalindrome(212), is(true));
    }

    @Test
    public void isPalindromeUseToString() throws Exception {
        assertThat(PalindromeNumber.isPalindromeUseToString(1), is(true));
        assertThat(PalindromeNumber.isPalindromeUseToString(-1), is(false));
        assertThat(PalindromeNumber.isPalindromeUseToString(101), is(true));
    }

}