package com.xsj.leetcode.easy;

import org.junit.Test;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class NumberOf1BitsTest {

    @Test
    public void testHammingWeight() throws Exception {
        assertThat(NumberOf1Bits.hammingWeight(8), equalTo(1));
        assertThat(NumberOf1Bits.hammingWeight(0xfffff), equalTo(20));
        assertThat(NumberOf1Bits.hammingWeight(-1), equalTo(32));
    }

}