package com.xsj.leetcode.easy;

import org.junit.Test;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

/**
 * Created by dengx.
 * Date: 2018/1/24
 * Time: 22:55
 */
public class RemoveDuplicatesFromSortedArrayTest {

    @Test
    public void testRemoveDuplicates() throws Exception {
        int[] ints = {1, 1, 1, 2, 2, 2, 2, 3, 3};
        assertThat(RemoveDuplicatesFromSortedArray.removeDuplicates(ints), is(3));
        int[] dest = new int[3];
        System.arraycopy(ints, 0, dest, 0, 3);
        assertThat(dest, is(new int[]{1, 2, 3}));
    }
}