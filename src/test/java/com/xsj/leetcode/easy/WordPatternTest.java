package com.xsj.leetcode.easy;

import org.junit.Test;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class WordPatternTest {

    @Test
    public void testWordPattern() throws Exception {
        assertThat(WordPattern.wordPattern("aabb", "aa aa bb bb"), is(true));
        assertThat(WordPattern.wordPattern("abba", "no yes yes yes"), is(false));
    }

}