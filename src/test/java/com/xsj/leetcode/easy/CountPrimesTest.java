package com.xsj.leetcode.easy;

import org.junit.Test;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

/**
 * Created by dengx.
 * Date: 2017/11/12
 * Time: 21:24
 */
public class CountPrimesTest {

    @Test
    public void testCountPrimes01() throws Exception {
        // 小于
        assertThat(CountPrimes.countPrimes01(2), is(0));
        assertThat(CountPrimes.countPrimes01(10), is(4));
    }


    @Test
    public void testCountPrimes02() throws Exception {
        assertThat(CountPrimes.countPrimes02(2), is(0));
        assertThat(CountPrimes.countPrimes02(10), is(4));
    }
}