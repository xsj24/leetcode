package com.xsj.leetcode.easy;

import org.junit.Test;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class FibonacciNumberTest {

    @Test
    public void fib() throws Exception {
        assertThat(FibonacciNumber.fib(1), is(1));
        assertThat(FibonacciNumber.fib(2), is(1));
        assertThat(FibonacciNumber.fib(3), is(2));
    }

}