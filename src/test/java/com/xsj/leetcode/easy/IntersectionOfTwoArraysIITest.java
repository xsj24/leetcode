package com.xsj.leetcode.easy;

import org.junit.Test;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class IntersectionOfTwoArraysIITest {

    @Test
    public void testIntersect() throws Exception {
        int[] res = IntersectionOfTwoArraysII.intersect(new int[]{1, 2, 2, 3}, new int[]{2, 2});
        assertThat(res, equalTo(new int[]{2, 2}));
    }

    @Test
    public void testIntersect_ii() throws Exception {
        int[] res = IntersectionOfTwoArraysII.intersect_ii(new int[]{1, 2, 2, 3}, new int[]{2, 2});
        assertThat(res, equalTo(new int[]{2, 2}));
    }
}