package com.xsj.leetcode.easy;

import org.junit.Test;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class ClimbingStairsTest {

    @Test
    public void climbStairs() throws Exception {
        assertThat(ClimbingStairs.climbStairs(2),  is(2));
        assertThat(ClimbingStairs.climbStairs(3),  is(3));
        assertThat(ClimbingStairs.climbStairs(4),  is(5));
    }

}