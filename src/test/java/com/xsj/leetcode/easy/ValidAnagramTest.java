package com.xsj.leetcode.easy;

import org.junit.Test;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class ValidAnagramTest {

    @Test
    public void isAnagram() throws Exception {
        assertThat(ValidAnagram.isAnagram("anagram", "nagaram"), is(true));
        assertThat(ValidAnagram.isAnagram("aaa", "bbb"), is(false));
    }

}