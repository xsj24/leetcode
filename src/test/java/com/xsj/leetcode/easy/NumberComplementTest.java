package com.xsj.leetcode.easy;

import org.junit.Test;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class NumberComplementTest {

    @Test
    public void testFindComplement() throws Exception {
        assertThat(NumberComplement.findComplement(5), equalTo(2));
    }
}