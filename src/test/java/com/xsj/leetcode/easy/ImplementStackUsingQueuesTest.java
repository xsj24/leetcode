package com.xsj.leetcode.easy;

import org.junit.Test;

import static com.xsj.leetcode.easy.ImplementStackUsingQueues.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;


public class ImplementStackUsingQueuesTest {

    @Test
    public void testMyStack() {
        MyStack myStack = new MyStack();
        myStack.push(21);
        myStack.push(-9);
        assertThat(myStack.top(), equalTo(-9));
        assertThat(myStack.pop(), equalTo(-9));
        assertThat(myStack.pop(), equalTo(21));
        assertThat(myStack.empty(), is(true));
    }


}