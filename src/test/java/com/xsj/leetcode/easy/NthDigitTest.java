package com.xsj.leetcode.easy;

import org.junit.Test;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class NthDigitTest {

    @Test
    public void testFindNthDigit() throws Exception {
        assertThat(NthDigit.findNthDigit(1), equalTo(1));
        assertThat(NthDigit.findNthDigit(150), equalTo(8));
    }
}