package com.xsj.leetcode.easy;

import org.junit.Test;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class MajorityElementTest {

    @Test
    public void testMajorityElementUsingMap() throws Exception {
        assertThat(MajorityElement.majorityElementUsingMap(new int[]{3, 2, 3, 3, 3}), is(3));
        assertThat(MajorityElement.majorityElementUsingMap(new int[]{5, 5, 5, 5, 5, 2, 2, 3, 3, 3, 5}), is(5));
    }


    @Test
    public void testMajorityElementRandomly() throws Exception {
        assertThat(MajorityElement.majorityElementRandomly(new int[]{3, 2, 3, 3, 3}), is(3));
        assertThat(MajorityElement.majorityElementRandomly(new int[]{5, 5, 5, 5, 5, 2, 2, 3, 3, 3, 5}), is(5));
    }


    @Test
    public void testMajorityElement() throws Exception {
        assertThat(MajorityElement.majorityElement(new int[]{2, 2, 3, 3, 3}), is(3));
        assertThat(MajorityElement.majorityElement(new int[]{5, 5, 5, 5, 5, 2, 2, 3, 3, 3, 5}), is(5));
    }

    @Test
    public void testMajorityElementUsingSort() throws Exception {
        assertThat(MajorityElement.majorityElementUsingSort(new int[]{2, 2, 3, 3, 3}), is(3));
        assertThat(MajorityElement.majorityElementUsingSort(new int[]{5, 5, 5, 5, 5, 2, 2, 3, 3, 3, 5}), is(5));
    }


}