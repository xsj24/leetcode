package com.xsj.leetcode.easy;

import org.junit.Test;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class IntersectionOfTwoArraysTest {

    @Test
    public void testIntersection01() throws Exception {
        int[] num1 = {1, 2, 2, 3};
        int[] num2 = {2, 2};
        int[] ints = IntersectionOfTwoArrays.intersection01(num1, num2);
        assertThat(ints, equalTo(new int[]{2}));
    }
}