package com.xsj.leetcode.easy;

import org.junit.Before;
import org.junit.Test;
import static com.xsj.leetcode.easy.ReverseLinkedList.*;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class ReverseLinkedListTest {

    ListNode head = null;

    @Before
    public void before() {
        int[] datas = {2, 3, 4, 5, 6};
        ListNode dummyHead = new ListNode(0);
        ListNode p = dummyHead;
        for (int data : datas) {
            p.next = new ListNode(data);
            p = p.next;
        }
        head = dummyHead.next;
    }


    @Test
    public void testReverseList() {
        ListNode p =  reverseList(head);
        int count = 0;
        ListNode tmp = p;
        while (tmp != null) {
            ++count;
            tmp = tmp.next;
        }
        assertThat(count, equalTo(5));
    }
}