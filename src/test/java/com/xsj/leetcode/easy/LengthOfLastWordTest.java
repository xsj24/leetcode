package com.xsj.leetcode.easy;

import org.junit.Test;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class LengthOfLastWordTest {

    @Test
    public void testLengthOfLastWord() throws Exception {
        assertThat(LengthOfLastWord.lengthOfLastWord("    "), equalTo(0));
        assertThat(LengthOfLastWord.lengthOfLastWord(" 1 "), equalTo(1));
        assertThat(LengthOfLastWord.lengthOfLastWord(" word"), equalTo(4));
    }
}