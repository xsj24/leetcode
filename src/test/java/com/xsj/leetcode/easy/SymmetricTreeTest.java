package com.xsj.leetcode.easy;

import com.xsj.leetcode.util.TreeNode;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class SymmetricTreeTest {

    @Test
    public void isSymmetric() {
        {
            TreeNode left = new TreeNode(2, new TreeNode(3), new TreeNode(4));
            TreeNode right = new TreeNode(2, new TreeNode(4), new TreeNode(3));
            TreeNode root = new TreeNode(1, left, right);
            assertThat(SymmetricTree.isSymmetric(root), is(true));
        }
    }


}