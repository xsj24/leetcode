package com.xsj.leetcode.easy;

import org.junit.Test;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class ValidParenthesesTest {

    @Test
    public void testIsValid() throws Exception {
        assertThat(ValidParentheses.isValid(""), equalTo(true));
        assertThat(ValidParentheses.isValid("[][]{}"), equalTo(true));
        assertThat(ValidParentheses.isValid("(([]{}([{()}])))"), equalTo(true));
    }

    @Test
    public void testIsValid_ii() {
        assertThat(ValidParentheses.isValid_ii(""), equalTo(true));
        assertThat(ValidParentheses.isValid_ii("[][]{}"), equalTo(true));
        assertThat(ValidParentheses.isValid_ii("(([]{}([{()}])))"), equalTo(true));
    }
}