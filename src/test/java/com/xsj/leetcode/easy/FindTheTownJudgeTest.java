package com.xsj.leetcode.easy;

import org.junit.Test;


import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;


public class FindTheTownJudgeTest {


    @Test
    public void findJudge() throws Exception {
        int[][] trust = {{1, 2}};
        assertThat(FindTheTownJudge.findJudge(2, trust), is(2));
        trust = new int[][]{{1, 3}, {3, 1}};
        assertThat(FindTheTownJudge.findJudge(3, trust), is(-1));
    }

}