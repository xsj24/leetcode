package com.xsj.interview.array;

import org.junit.Test;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class CommonsTest {

    @Test
    public void partition() {
        assertThat(Commons.partition(new int[]{4, 3, 2, 1}), is(3));
        assertThat(Commons.partition(new int[]{0, -1, -2, -3, -4}), is(-1));
        assertThat(Commons.partition(new int[]{-1, -2, 3, -5, 6, 0}), is(1));
    }
}