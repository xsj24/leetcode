package com.xsj.interview.array;

import org.junit.Test;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class RotateMatrixTest {

    @Test
    public void rotate() {
        int[][] arr = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        RotateMatrix.rotate(arr);
        assertThat(arr, is(new int[][]{{7, 4, 1}, {8, 5, 2}, {9, 6, 3}}));
    }

}