package com.xsj.interview.string;

import com.xsj.leetcode.util.ListHelper;
import com.xsj.leetcode.util.ListNode;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class PalindromeCheckTest {

    @Test
    public void isPalindrome() {
        assertThat(PalindromeCheck.isPalindrome("a"), is(true));
        assertThat(PalindromeCheck.isPalindrome("cc"), is(true));
        assertThat(PalindromeCheck.isPalindrome("ab"), is(false));
        assertThat(PalindromeCheck.isPalindromeRecursively("acbca"), is(true));
    }

    @Test
    public void testLink() {
        ListNode<Integer> node = ListHelper.create(1, 2, 3, 3, 2, 1);
        assertThat(PalindromeCheck.isPalindrome(node), is(true));
    }
}
