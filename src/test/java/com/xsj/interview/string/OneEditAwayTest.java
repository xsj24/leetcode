package com.xsj.interview.string;

import org.junit.Test;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class OneEditAwayTest {


    @Test
    public void solute01() throws Exception {
        assertThat(OneEditAway.solute01("", "1"), is(true));
        assertThat(OneEditAway.solute01("pale", "ple"), is(true));
        assertThat(OneEditAway.solute01("pale", "plea"), is(false));
        assertThat(OneEditAway.solute01("pale", "bale"), is(true));
        assertThat(OneEditAway.solute01("ab", "abab"), is(false));

        assertThat(OneEditAway.solute01("abc", "abdd"), is(false));
    }

}