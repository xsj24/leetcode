package com.xsj.interview.string;

import org.junit.Test;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;


public class StringCompressionTest {

    @Test
    public void compress() throws Exception {
        assertThat(StringCompression.compress(""), is(""));
        assertThat(StringCompression.compress("aaa"), is("a3"));
        assertThat(StringCompression.compress("abab"), is("abab"));
        assertThat(StringCompression.compress("aabb"), is("aabb"));
    }


}