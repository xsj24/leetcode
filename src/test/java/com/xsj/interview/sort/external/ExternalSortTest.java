package com.xsj.interview.sort.external;

import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;

import static org.junit.Assert.*;

public class ExternalSortTest {

    @Test
    public void testMain() throws IOException {
        String storeDir = "E:\\coding\\leetcode\\stored";
        String[] arg = {"-d", "-z", "-v", "-s", storeDir, "E:\\coding\\leetcode\\s1.txt", "./target/gg.txt"};
        ExternalSort.main(arg);
    }
}